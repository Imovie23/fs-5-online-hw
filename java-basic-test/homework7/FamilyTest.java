package homework7;

import org.junit.jupiter.api.*;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DisplayName("Family homework7")
public class FamilyTest {
    @Mock
    private Human bart;
    @Mock
    private Man homer;
    @Mock
    private Woman marge;
    @Mock
    private Human lisa;
    @InjectMocks
    private Family family;

    private final PrintStream standardOut = System.out;
    private final ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();

    @BeforeEach
    void setUp() {
        System.setOut(new PrintStream(outputStreamCaptor));

        homer = Mockito.mock(Man.class);
        marge = Mockito.mock(Woman.class);
        bart = Mockito.mock(Human.class);
        lisa = Mockito.mock(Human.class);

        family = new Family(marge, homer);
    }

    @AfterEach
    public void tearDown() {
        System.setOut(standardOut);
    }

    @DisplayName("Checking the equality of the toString method")
    @Test
    public void testToString() {
        Mockito.when(homer.toString()).thenReturn("Homer");
        Mockito.when(marge.toString()).thenReturn("Marge");
        Mockito.when(bart.toString()).thenReturn("Bart");

        family.addChild(bart);

        String expected = "Family{"
                + "mother=" + "Marge" + ", "
                + "father=" + "Homer" + ", "
                + "children=" + "[Bart]" + ", "
                + "pet=" + null
                + "}";

        assertEquals(expected, family.toString());
    }

    @DisplayName("Checking the count after adding a child to the family")
    @Test
    public void testAddChildCount() {
        assertEquals(0, family.getChildren().size());

        family.addChild(bart);

        assertEquals(1, family.getChildren().size());
    }

    @DisplayName("Checking that the added child belongs to this family")
    @Test
    public void testAddChildBelongsToThisFamily() {
        Mockito.doCallRealMethod().when(bart).setFamily(Mockito.any(Family.class));
        Mockito.doCallRealMethod().when(bart).getFamily();

        family.addChild(bart);

        assertEquals(family, bart.getFamily());
    }

    @DisplayName("Checking the count after adding a child to a family is done by the countFamily method")
    @Test
    public void testCountFamily() {
        assertEquals(2, family.countFamily());

        family.addChild(bart);

        assertEquals(3, family.countFamily());
    }

    @DisplayName("Checking for child element deletion at out-of-range index")
    @Test
    public void testDeleteChildWhenElementOutOfRangeIndex() {
        boolean isDeleted = family.deleteChild(1);

        Assertions.assertFalse(isDeleted);
    }

    @DisplayName("Error checking for child element deletion with the correct index")
    @Test
    public void testDeleteChildWhenElementWithTheCorrectIndex() {
        family.addChild(bart);

        boolean isDeleted = family.deleteChild(0);

        Assertions.assertTrue(isDeleted);
    }

    @DisplayName("Error checking child element that does not belong to a family")
    @Test
    public void testDeleteChildWhenElementNotBelongToFamily() {
        family.addChild(bart);

        boolean isDeleted = family.deleteChild(lisa);

        Assertions.assertFalse(isDeleted);
        assertEquals("This child does not belong to this family", outputStreamCaptor.toString().trim());
        assertEquals(1, family.getChildren().size());
    }

    @DisplayName("Checking a child element that belongs to a family")
    @Test
    public void testDeleteChildWhenThatBelongFamily() {
        Mockito.doCallRealMethod().when(bart).setFamily(Mockito.any(Family.class));
        Mockito.doCallRealMethod().when(bart).getFamily();

        family.addChild(bart);

        assertEquals(3, family.countFamily());

        boolean isDeleted = family.deleteChild(bart);

        Assertions.assertTrue(isDeleted);
        assertEquals(2, family.countFamily());
        assertEquals(0, family.getChildren().size());
    }

    @DisplayName("Checking a born child that instance of Human")
    @Test
    public void testBornChildInstanceOfHuman() {
        Human child = family.bornChild();

        Assertions.assertInstanceOf(Human.class, child);
    }

    @DisplayName("Checking of a born child is equal to a family")
    @Test
    public void testBornChildEqualsFamily() {
        Human child = family.bornChild();

        assertEquals(child.getFamily().toString(), family.toString());
    }

    @DisplayName("Checking the characteristics of a born child")
    @Test
    public void testBornChildСharacteristics() {
        String surname = "Simpsons";
        int iq = 50;

        Mockito.when(homer.getIq()).thenReturn(iq);
        Mockito.when(marge.getIq()).thenReturn(iq);

        Mockito.when(homer.getSurname()).thenReturn(surname);

        Human child = family.bornChild();

        assertEquals(iq, child.getIq());
        assertEquals(surname, child.getSurname());
    }

    @DisplayName("Checking the number of people in the family after the birth of a child")
    @Test
    public void testBornChildCountOfFamily() {
        Human child = family.bornChild();

        assertEquals(3, child.getFamily().countFamily());
    }

}
