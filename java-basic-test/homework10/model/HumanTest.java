package homework10.model;

import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
@DisplayName("Human homework10")
public class HumanTest {
    private Human humanWithoutFamily;
    private Human humanWithFamily;
    @Mock
    private Man man;
    @Mock
    private Woman woman;
    @Mock
    private List<AbstractPet> petListMock;
    @InjectMocks
    private Family familyMock;
    private AbstractPet petMock;

    private final PrintStream standardOut = System.out;
    private final ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();
    private final String ERROR_MESSAGE = "There is no pet in the family!!!";

    @BeforeEach
    void setUp() {
        System.setOut(new PrintStream(outputStreamCaptor));

        Map<String, Set<String>> homerSchedule = new HashMap<>();

        homerSchedule.put(DayOfWeek.MONDAY.name(), new HashSet<>(List.of("Drink beer")));
        homerSchedule.put(DayOfWeek.FRIDAY.name(), new HashSet<>(List.of("Drink beer")));



        humanWithoutFamily = new Human("Homer", "Simpson", "10/11/1987", 55,
                homerSchedule);



        humanWithFamily = new Human("Homer", "Simpson", "10/11/1987", 55,
                homerSchedule);

        petMock = Mockito.mock(AbstractPet.class);

        petListMock = new ArrayList<>(List.of(petMock));

        familyMock = Mockito.mock(Family.class);

        humanWithFamily.setFamily(familyMock);
    }

    @AfterEach
    public void tearDown() {
        System.setOut(standardOut);
    }

    @DisplayName("Checking the equality of the toString method")
    @Test
    public void testToString() {

        String expected = "Human{"
                + "name='" + "Homer" + "', "
                + "surname='" + "Simpson" + "', "
                + "birthDate=" + "10/11/1987" + ", "
                + "iq=" + 55 + ", "
                + "schedule=" + "{MONDAY=[Drink beer], FRIDAY=[Drink beer]}"
                + "}";

        assertEquals(expected, humanWithoutFamily.toString());
    }

    @DisplayName("Checking message for the greet pet method with family")
    @Test
    public void testGreetPet() {
        Mockito.when(familyMock.getPets()).thenReturn(petListMock);
        Mockito.when(petMock.getNickname()).thenReturn("Santa's Little Helper");

        humanWithFamily.greetPets();

        String expectedMassage = "Привіт, Santa's Little Helper";

        assertEquals(expectedMassage, outputStreamCaptor.toString().trim());
    }

    @DisplayName("Error checking for the greet pet method without family")
    @Test
    public void testGreetPetFailed() {
        humanWithoutFamily.greetPets();

        assertEquals(ERROR_MESSAGE, outputStreamCaptor.toString().trim());
    }

    @DisplayName("Checking message for the describe with trick level less then 50 pet method with family")
    @Test
    public void testDescribePetTrickLevelLessThenFifty() {
        Mockito.when(familyMock.getPets()).thenReturn(petListMock);
        Mockito.when(petMock.getSpecies()).thenReturn(Species.DOG);
        Mockito.when(petMock.getAge()).thenReturn(10);
        Mockito.when(petMock.getTrickLevel()).thenReturn(40);

        humanWithFamily.describePets();

        String expectedMassage = String.format("У мене є %s, їй %d років, він %s",
                Species.DOG, 10, "майже не хитрий");

        assertEquals(expectedMassage, outputStreamCaptor.toString().trim());
    }

    @DisplayName("Checking message for the describe with trick level greater then 50 pet method with family")
    @Test
    public void testDescribePetTrickLevelGreaterThenFifty() {
        Mockito.when(familyMock.getPets()).thenReturn(petListMock);
        Mockito.when(petMock.getSpecies()).thenReturn(Species.DOG);
        Mockito.when(petMock.getAge()).thenReturn(10);
        Mockito.when(petMock.getTrickLevel()).thenReturn(55);

        humanWithFamily.describePets();

        String expectedMassage = String.format("У мене є %s, їй %d років, він %s",
                Species.DOG, 10, "дуже хитрий");

        assertEquals(expectedMassage, outputStreamCaptor.toString().trim());
    }

    @DisplayName("Error checking for the describe pet method without family")
    @Test
    public void testDescribePetFailed() {
        humanWithoutFamily.describePets();

        assertEquals(ERROR_MESSAGE, outputStreamCaptor.toString().trim());
    }

    @DisplayName("Error checking for the feed pet method without family")
    @Test
    public void testFeedPetFailed() {
        humanWithoutFamily.describePets();

        assertEquals(ERROR_MESSAGE, outputStreamCaptor.toString().trim());
    }

    @DisplayName("Checking the family has pet method without a family")
    @Test
    public void testFamilyHasPetWithoutFamily() {
        boolean hasPet = humanWithoutFamily.familyHasPets();

        Assertions.assertFalse(hasPet);
    }

    @DisplayName("Checking the family has pet method with a family")
    @Test
    public void testFamilyHasPetWithFamily() {
        Mockito.when(familyMock.getPets()).thenReturn(petListMock);
        boolean hasPet = humanWithFamily.familyHasPets();

        Assertions.assertTrue(hasPet);
    }

    @DisplayName("Checking the message of notify pet message method")
    @Test
    public void testNotifyPetMessage() {
        humanWithFamily.notifyPetMessage();
        String expectedMessage = "There is no pet in the family!!!";

        assertEquals(expectedMessage, outputStreamCaptor.toString().trim());
    }


}
