package homework10.model;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.HashSet;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DisplayName("DomesticCat homework10")
public class DomesticCatTest {
    private DomesticCat cat;
    private final PrintStream standardOut = System.out;
    private final ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();

    @BeforeEach
    void setUp() {
        System.setOut(new PrintStream(outputStreamCaptor));

        cat = new DomesticCat("Snowball V", 5, 13,
                new HashSet<>(List.of("Sleep")));
    }

    @AfterEach
    public void tearDown() {
        System.setOut(standardOut);
    }

    @DisplayName("Checking the equality of the toString method")
    @Test
    public void testToString() {

        String expected = Species.DOMESTIC_CAT + "{"
                + "nickname='" + "Snowball V" + "', "
                + "age=" + 5 + ", "
                + "trickLevel=" + 13 + ", "
                + "habits=" + "[Sleep]"
                + "}";

        assertEquals(expected, cat.toString());
    }

    @DisplayName("Checking for the eat method")
    @Test
    public void testEat() {
        cat.eat();
        String expectedMessage = "I'm eating!";

        assertEquals(expectedMessage, outputStreamCaptor.toString().trim());
    }

    @DisplayName("Checking for the respond method")
    @Test
    public void testRespond() {
        cat.respond();
        String expectedMessage = String.format("Hello, host. I am a %s named %s.",
                cat.getSpecies(), cat.getNickname());

        assertEquals(expectedMessage, outputStreamCaptor.toString().trim());
    }

    @DisplayName("Checking for the foul method")
    @Test
    public void testFoul() {
        cat.foul();
        String expectedMessage = "Need to escape...";

        assertEquals(expectedMessage, outputStreamCaptor.toString().trim());
    }

}
