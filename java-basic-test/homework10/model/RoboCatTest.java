package homework10.model;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.HashSet;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DisplayName("RoboCat homework10")
public class RoboCatTest {
    private RoboCat roboCat;
    private final PrintStream standardOut = System.out;
    private final ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();

    @BeforeEach
    void setUp() {
        System.setOut(new PrintStream(outputStreamCaptor));

        roboCat = new RoboCat("Robopet", 1000, 100,
                new HashSet<>(List.of("The charge settles")));
    }

    @AfterEach
    public void tearDown() {
        System.setOut(standardOut);
    }

    @DisplayName("Checking the equality of the toString method")
    @Test
    public void testToString() {

        String expected = Species.ROBO_CAT + "{"
                + "nickname='" + "Robopet" + "', "
                + "age=" + 1000 + ", "
                + "trickLevel=" + 100 + ", "
                + "habits=" + "[The charge settles]"
                + "}";

        assertEquals(expected, roboCat.toString());
    }

    @DisplayName("Checking for the eat method")
    @Test
    public void testEat() {
        roboCat.eat();
        String expectedMessage = "I'm eating!";

        assertEquals(expectedMessage, outputStreamCaptor.toString().trim());
    }

    @DisplayName("Checking for the respond method")
    @Test
    public void testRespond() {
        roboCat.respond();
        String expectedMessage = String.format("Hello, host. I am a %s named %s.",
                roboCat.getSpecies(), roboCat.getNickname());

        assertEquals(expectedMessage, outputStreamCaptor.toString().trim());
    }

}
