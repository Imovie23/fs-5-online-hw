package homework5;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DisplayName("Pet homework5")
public class PetTest {
    private Pet pet;
    private final PrintStream standardOut = System.out;
    private final ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();

    @BeforeEach
    void setUp() {
        System.setOut(new PrintStream(outputStreamCaptor));

        pet = new Pet(Species.DOG, "Santa's Little Helper", 10, 50,
                new String[]{
                        "Sleep"
                });
    }

    @AfterEach
    public void tearDown() {
        System.setOut(standardOut);
    }

    @DisplayName("Checking the equality of the toString method")
    @Test
    public void testToString() {

        String expected = Species.DOG + "{"
                + "nickname='" + "Santa's Little Helper" + "', "
                + "age=" + 10 + ", "
                + "trickLevel=" + 50 + ", "
                + "habits=" + "[Sleep]"
                + "}";

        assertEquals(expected, pet.toString());
    }

    @DisplayName("Checking for the eat method")
    @Test
    public void testEat() {
        pet.eat();
        String expectedMessage = "Я ї'м!";

        assertEquals(expectedMessage, outputStreamCaptor.toString().trim());
    }

    @DisplayName("Checking for the respond method")
    @Test
    public void testRespond() {
        pet.respond();
        String expectedMessage = "Привіт, хазяїн. Я - Santa's Little Helper. Я скучив!";

        assertEquals(expectedMessage, outputStreamCaptor.toString().trim());
    }

    @DisplayName("Checking for the foul method")
    @Test
    public void testFoul() {
        pet.foul();
        String expectedMessage = "Потрібно добре замести сліди...";

        assertEquals(expectedMessage, outputStreamCaptor.toString().trim());
    }

}
