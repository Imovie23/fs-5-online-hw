package homework5;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DisplayName("Family homework5")
public class FamilyTest {
    @Mock
    private Human bart;
    @Mock
    private Human homer;
    @Mock
    private Human marge;
    @Mock
    private Human lisa;
    @InjectMocks
    private Family family;

    private final PrintStream standardOut = System.out;
    private final ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();

    @BeforeEach
    void setUp() {
        System.setOut(new PrintStream(outputStreamCaptor));

        homer = Mockito.mock(Human.class);
        marge = Mockito.mock(Human.class);
        bart = Mockito.mock(Human.class);
        lisa = Mockito.mock(Human.class);

        family = new Family(homer, marge);
    }

    @AfterEach
    public void tearDown() {
        System.setOut(standardOut);
    }

    @DisplayName("Checking the equality of the toString method")
    @Test
    public void testToString() {
        Mockito.when(homer.toString()).thenReturn("Homer");
        Mockito.when(marge.toString()).thenReturn("Marge");
        Mockito.when(bart.toString()).thenReturn("Bart");

        family.addChild(bart);

        String expected = "Family{"
                + "mother=" + "Homer" + ", "
                + "father=" + "Marge" + ", "
                + "children=" + "[Bart]" + ", "
                + "pet=" + null
                + "}";

        assertEquals(expected, family.toString());
    }

    @DisplayName("Checking the count after adding a child to the family")
    @Test
    public void testAddChildCount() {
        assertEquals(0, family.getChildren().length);

        family.addChild(bart);

        assertEquals(1, family.getChildren().length);
    }

    @DisplayName("Checking that the added child belongs to this family")
    @Test
    public void testAddChildBelongsToThisFamily() {
        Mockito.doCallRealMethod().when(bart).setFamily(Mockito.any(Family.class));
        Mockito.doCallRealMethod().when(bart).getFamily();

        family.addChild(bart);

        assertEquals(family, bart.getFamily());
    }

    @DisplayName("Checking the count after adding a child to a family is done by the countFamily method")
    @Test
    public void testCountFamily() {
        assertEquals(2, family.countFamily());

        family.addChild(bart);

        assertEquals(3, family.countFamily());
    }

    @DisplayName("Checking for child element deletion at out-of-range index")
    @Test
    public void testDeleteChildWhenElementOutOfRangeIndex() {
        boolean isDeleted = family.deleteChild(1);

        Assertions.assertFalse(isDeleted);
    }

    @DisplayName("Error checking for child element deletion with the correct index")
    @Test
    public void testDeleteChildWhenElementWithTheCorrectIndex() {
        family.addChild(bart);

        boolean isDeleted = family.deleteChild(0);

        Assertions.assertTrue(isDeleted);
    }

    @DisplayName("Error checking child element that does not belong to a family")
    @Test
    public void testDeleteChildWhenElementNotBelongToFamily() {
        family.addChild(bart);

        boolean isDeleted = family.deleteChild(lisa);

        Assertions.assertFalse(isDeleted);
        assertEquals("This child does not belong to this family", outputStreamCaptor.toString().trim());
        assertEquals(1, family.getChildren().length);
    }

    @DisplayName("Checking a child element that belongs to a family")
    @Test
    public void testDeleteChildWhenThatBelongFamily() {
        Mockito.doCallRealMethod().when(bart).setFamily(Mockito.any(Family.class));
        Mockito.doCallRealMethod().when(bart).getFamily();

        family.addChild(bart);

        assertEquals(3, family.countFamily());

        boolean isDeleted = family.deleteChild(bart);

        Assertions.assertTrue(isDeleted);
        assertEquals(2, family.countFamily());
        assertEquals(0, family.getChildren().length);
    }

}
