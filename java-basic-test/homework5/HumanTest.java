package homework5;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
@DisplayName("Human homework5")
public class HumanTest {
    @Mock
    private Human humanWithoutFamily;
    @Mock
    private Human humanWithFamily;
    @Mock
    private Pet petMock;
    @InjectMocks
    private Family familyMock;


    private final PrintStream standardOut = System.out;
    private final ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();
    private final String ERROR_MESSAGE = "There is no pet in the family!!!";

    @BeforeEach
    void setUp() {
        System.setOut(new PrintStream(outputStreamCaptor));

        humanWithoutFamily = new Human("Homer", "Simpson", 1987, 55,
                new String[][]{
                        {DayOfWeek.MONDAY.name(), "Drink beer"},
                        {DayOfWeek.FRIDAY.name(), "Drink beer"}
                });

        humanWithFamily = new Human("Homer", "Simpson", 1987, 55,
                new String[][]{
                        {DayOfWeek.MONDAY.name(), "Drink beer"},
                        {DayOfWeek.FRIDAY.name(), "Drink beer"}
                });

        petMock = Mockito.mock(Pet.class);

        familyMock = Mockito.mock(Family.class);

        humanWithFamily.setFamily(familyMock);
    }

    @AfterEach
    public void tearDown() {
        System.setOut(standardOut);
    }

    @DisplayName("Checking the equality of the toString method")
    @Test
    public void testToString() {

        String expected = "Human{"
                + "name='" + "Homer" + "', "
                + "surname='" + "Simpson" + "', "
                + "year=" + 1987 + ", "
                + "iq=" + 55 + ", "
                + "schedule=" + "[[MONDAY, Drink beer], [FRIDAY, Drink beer]]"
                + "}";

        assertEquals(expected, humanWithoutFamily.toString());
    }

    @DisplayName("Checking message for the greet pet method with family")
    @Test
    public void testGreetPet() {
        Mockito.when(familyMock.getPet()).thenReturn(petMock);
        Mockito.when(petMock.getNickname()).thenReturn("Santa's Little Helper");

        humanWithFamily.greetPet();

        String expectedMassage = "Привіт, Santa's Little Helper";

        assertEquals(expectedMassage, outputStreamCaptor.toString().trim());
    }

    @DisplayName("Error checking for the greet pet method without family")
    @Test
    public void testGreetPetFailed() {
        humanWithoutFamily.greetPet();

        assertEquals(ERROR_MESSAGE, outputStreamCaptor.toString().trim());
    }

    @DisplayName("Checking message for the describe with trick level less then 50 pet method with family")
    @Test
    public void testDescribePetTrickLevelLessThenFifty() {
        Mockito.when(familyMock.getPet()).thenReturn(petMock);
        Mockito.when(petMock.getSpecies()).thenReturn(Species.DOG);
        Mockito.when(petMock.getAge()).thenReturn(10);
        Mockito.when(petMock.getTrickLevel()).thenReturn(40);

        humanWithFamily.describePet();

        String expectedMassage = String.format("У мене є %s, їй %d років, він %s",
                Species.DOG, 10, "майже не хитрий");

        assertEquals(expectedMassage, outputStreamCaptor.toString().trim());
    }

    @DisplayName("Checking message for the describe with trick level greater then 50 pet method with family")
    @Test
    public void testDescribePetTrickLevelGreaterThenFifty() {
        Mockito.when(familyMock.getPet()).thenReturn(petMock);
        Mockito.when(petMock.getSpecies()).thenReturn(Species.DOG);
        Mockito.when(petMock.getAge()).thenReturn(10);
        Mockito.when(petMock.getTrickLevel()).thenReturn(55);

        humanWithFamily.describePet();

        String expectedMassage = String.format("У мене є %s, їй %d років, він %s",
                Species.DOG, 10, "дуже хитрий");

        assertEquals(expectedMassage, outputStreamCaptor.toString().trim());
    }

    @DisplayName("Error checking for the describe pet method without family")
    @Test
    public void testDescribePetFailed() {
        humanWithoutFamily.describePet();

        assertEquals(ERROR_MESSAGE, outputStreamCaptor.toString().trim());
    }

//    @DisplayName("Checking message for the feed pet trick level greater then random number method with family")
//    @Test
//    public void testFeedPetWithLevelGreaterThenRandom() {
//
//       try(MockedStatic<Math> math = Mockito.mockStatic(Math.class)) {
//           math.when(Math::random).thenReturn(0.1);
//       }
//
//        Mockito.when(familyMock.getPet()).thenReturn(petMock);
//        Mockito.when(petMock.getNickname()).thenReturn("Santa's Little Helper");
//        Mockito.when(petMock.getTrickLevel()).thenReturn(40);
//
//        boolean expectedFeed = humanWithFamily.feedPet(false);
//
//        String expectedMassage = String.format("Хм... годувати %s", "Santa's Little Helper");
//
//        Assertions.assertTrue(expectedFeed);
//    }


    @DisplayName("Error checking for the feed pet method without family")
    @Test
    public void testFeedPetFailed() {
        humanWithoutFamily.describePet();

        assertEquals(ERROR_MESSAGE, outputStreamCaptor.toString().trim());
    }


}
