package homework11.model;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.HashSet;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DisplayName("Dog homework11")
public class DogTest {
    private Dog dog;
    private final PrintStream standardOut = System.out;
    private final ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();

    @BeforeEach
    void setUp() {
        System.setOut(new PrintStream(outputStreamCaptor));

        dog = new Dog("Santa's Little Helper", 10, 50,
                new HashSet<>(List.of("Sleep")));
    }

    @AfterEach
    public void tearDown() {
        System.setOut(standardOut);
    }

    @DisplayName("Checking the equality of the toString method")
    @Test
    public void testToString() {

        String expected = Species.DOG + "{"
                + "nickname='" + "Santa's Little Helper" + "', "
                + "age=" + 10 + ", "
                + "trickLevel=" + 50 + ", "
                + "habits=" + "[Sleep]"
                + "}";

        assertEquals(expected, dog.toString());
    }

    @DisplayName("Checking for the eat method")
    @Test
    public void testEat() {
        dog.eat();
        String expectedMessage = "I'm eating!";

        assertEquals(expectedMessage, outputStreamCaptor.toString().trim());
    }

    @DisplayName("Checking for the respond method")
    @Test
    public void testRespond() {
        dog.respond();
        String expectedMessage = String.format("Hello, host. I am a %s named %s.",
                dog.getSpecies(), dog.getNickname());

        assertEquals(expectedMessage, outputStreamCaptor.toString().trim());
    }

    @DisplayName("Checking for the foul method")
    @Test
    public void testFoul() {
        dog.foul();
        String expectedMessage = "You need to cover your tracks well...";

        assertEquals(expectedMessage, outputStreamCaptor.toString().trim());
    }

}
