package homework11.utils;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.*;
import java.util.NoSuchElementException;
import java.util.Scanner;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ConsoleBirthDateTest {
    private final PrintStream standardOut = System.out;
    private final PrintStream standardErr = System.err;
    private final ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();
    private final ByteArrayOutputStream outputStreamErrorCaptor = new ByteArrayOutputStream();

    @BeforeEach
    void setUp() {
        System.setOut(new PrintStream(outputStreamCaptor));
        System.setErr(new PrintStream(outputStreamErrorCaptor));

    }

    @AfterEach
    public void tearDown() {
        System.setOut(standardOut);
        System.setOut(standardErr);
    }

    @DisplayName("Console request and validate year failed")
    @Test
    public void testConsoleRequestAndValidateYearFailed() {
        Scanner scanner = new Scanner(new ByteArrayInputStream("0".getBytes()));

        ConsoleBirthDate consoleBirthDate =
                new ConsoleBirthDate(scanner);


        try {
            consoleBirthDate.consoleRequestAndValidateYear();
        } catch (NoSuchElementException ignored) {

        }

        assertEquals("Incorrect number year, please try again", outputStreamErrorCaptor.toString().trim());
    }

    @DisplayName("Console request and validate year")
    @Test
    public void testConsoleRequestAndValidateYear() {
        int expected = 2023;

        Scanner scanner = new Scanner(new ByteArrayInputStream(String.valueOf(expected).getBytes()));

        ConsoleBirthDate consoleBirthDate =
                new ConsoleBirthDate(scanner);

        int year = consoleBirthDate.consoleRequestAndValidateYear();

        assertEquals(expected, year);
    }

    @DisplayName("Console request and validate year with max age of human failed")
    @Test
    public void testConsoleRequestAndValidateYearWithMaxAgeOfHumanFailed() {
        Scanner scanner = new Scanner(new ByteArrayInputStream("2020".getBytes()));

        ConsoleBirthDate consoleBirthDate =
                new ConsoleBirthDate(scanner, 2021);


        try {
            consoleBirthDate.consoleRequestAndValidateYear();
        } catch (NoSuchElementException ignored) {

        }

        assertEquals("Year of birth can start from 2021 to 2023", outputStreamErrorCaptor.toString().trim());
    }

    @DisplayName("Console request and validate month failed")
    @Test
    public void testConsoleRequestAndValidateMonthFailed() {
        Scanner scanner = new Scanner(new ByteArrayInputStream("13".getBytes()));

        ConsoleBirthDate consoleBirthDate =
                new ConsoleBirthDate(scanner);


        try {
            consoleBirthDate.consoleRequestAndValidateMonth();
        } catch (NoSuchElementException ignored) {

        }

        assertEquals("Incorrect number month, please try again", outputStreamErrorCaptor.toString().trim());
    }

    @DisplayName("Console request and validate month")
    @Test
    public void testConsoleRequestAndValidateMonth() {
        int expected = 12;

        Scanner scanner = new Scanner(new ByteArrayInputStream(String.valueOf(expected).getBytes()));

        ConsoleBirthDate consoleBirthDate =
                new ConsoleBirthDate(scanner);


       int month = consoleBirthDate.consoleRequestAndValidateMonth();

        assertEquals(expected, month);
    }

    @DisplayName("Console request and validate day failed")
    @Test
    public void testConsoleRequestAndValidateDayFailed() {
        Scanner scanner = new Scanner(new ByteArrayInputStream("100".getBytes()));

        ConsoleBirthDate consoleBirthDate =
                new ConsoleBirthDate(scanner);


        try {
            consoleBirthDate.consoleRequestAndValidateDay(1990, 12);
        } catch (NoSuchElementException ignored) {

        }

        assertEquals("Invalid day number, the selected month has a range of days from 1 to 31", outputStreamErrorCaptor.toString().trim());
    }

    @DisplayName("Console request and validate day")
    @Test
    public void testConsoleRequestAndValidateDay() {
        int expected = 10;

        Scanner scanner = new Scanner(new ByteArrayInputStream(String.valueOf(expected).getBytes()));

        ConsoleBirthDate consoleBirthDate =
                new ConsoleBirthDate(scanner);

        int day =  consoleBirthDate.consoleRequestAndValidateDay(1990, 12);

        assertEquals(expected, day);
    }
}
