package homework11.utils;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.NoSuchElementException;
import java.util.Scanner;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ConsoleUtilTest {
    private final PrintStream standardOut = System.out;
    private final ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();
    private final ByteArrayOutputStream outputStreamErrorCaptor = new ByteArrayOutputStream();
    private final String ERROR_MASSAGE = "Error message";

    @BeforeEach
    void setUp() {
        System.setOut(new PrintStream(outputStreamCaptor));
        System.setErr(new PrintStream(outputStreamErrorCaptor));
    }

    @AfterEach
    public void tearDown() {
        System.setOut(standardOut);
    }

    @DisplayName("Get input number value failed")
    @Test
    public void testGetInputNumberValueFailed() {
        Scanner scanner = new Scanner(new ByteArrayInputStream("test".getBytes()));


        try {
            ConsoleUtil.getInputNumberValue(scanner, "", ERROR_MASSAGE);
        } catch (NoSuchElementException ignored) {

        }

        assertEquals(ERROR_MASSAGE, outputStreamErrorCaptor.toString().trim());
    }

    @DisplayName("Get input number value")
    @Test
    public void testGetInputNumberValue() {
        int expected = 12;

        Scanner scanner = new Scanner(new ByteArrayInputStream(String.valueOf(expected).getBytes()));

       int value = ConsoleUtil.getInputNumberValue(scanner, "", ERROR_MASSAGE);

        assertEquals(expected, value);
    }

    @DisplayName("Get input number value with another params failed")
    @Test
    public void testGetInputNumberValueWithAnotherParamsFailed() {
        Scanner scanner = new Scanner(new ByteArrayInputStream("test".getBytes()));


        try {
            ConsoleUtil.getInputNumberValue(scanner, "");
        } catch (NoSuchElementException ignored) {

        }

        assertEquals("Wrong number entered, please try again", outputStreamErrorCaptor.toString().trim());
    }

    @DisplayName("Get input string value")
    @Test
    public void testGetInputStringValue() {
        String expected = "expected";

        Scanner scanner = new Scanner(new ByteArrayInputStream(expected.getBytes()));

        String value = ConsoleUtil.getInputStringValue(scanner, "", ERROR_MASSAGE);

        assertEquals(expected, value);
    }
}
