package homework11.controller;

import homework11.exception.FamilyIndexOutOfBoundsException;
import homework11.model.Family;
import homework11.model.Human;
import homework11.service.FamilyService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
public class FamilyControllerTest {

    @Mock
    private FamilyService mockFamilyService;
    @Mock
    private final Scanner mockScanner = Mockito.mock(Scanner.class);

    @InjectMocks
    private FamilyController familyController;

    @Mock
    private Human mockMan;
    @Mock
    private Human mockWoman;
    @Mock
    private homework11.model.Family mockFamily;

    private final PrintStream standardOut = System.out;
    private final PrintStream standardErr = System.err;
    private final ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();
    private final ByteArrayOutputStream outputErrorStreamCaptor = new ByteArrayOutputStream();

    @BeforeEach
    void setUp() {
        System.setOut(new PrintStream(outputStreamCaptor));
        System.setErr(new PrintStream(outputErrorStreamCaptor));
    }


    @AfterEach
    public void tearDown() {
        System.setOut(standardOut);
        System.setErr(standardErr);
    }


    @Test
    public void testAdoptChild() {
        Mockito.when(mockFamilyService.adoptChild(Mockito.any(), Mockito.any())).thenReturn(mockFamily);

        Family familyWithChild = familyController.adoptChild(mockFamily, mockMan);

        assertEquals(mockFamily, familyWithChild);
    }

    @Test
    public void testAdoptChildWithCatchFamilyOverflowException() {
        Mockito.when(mockScanner.nextLine()).thenReturn("0");

        Mockito.when(mockFamilyService.getAllFamilies()).thenReturn(new ArrayList<>(List.of(mockFamily)));

        Mockito.when(mockFamilyService.getFamilyById(Mockito.anyInt())).thenReturn(mockFamily);
        Mockito.when(mockFamily.countFamily()).thenReturn(10);

        familyController.adoptChild();

        assertEquals("It is not possible to add a child to this family. Limit exceeded",
                outputErrorStreamCaptor.toString().trim());

    }

    @Test
    public void testAdoptChildWithFamilyIndexOutOfBoundsException() {
        Mockito.when(mockScanner.nextLine()).thenReturn("1");

        familyController.adoptChild();

        assertEquals("Invalid family index",
                outputErrorStreamCaptor.toString().trim());

    }

    @Test
    public void testAdoptChildWithFamilyImpossibleBornChildException() {
        Mockito.when(mockScanner.nextLine()).thenReturn("0");

        Mockito.when(mockFamilyService.getAllFamilies()).thenReturn(new ArrayList<>(List.of(mockFamily)));

        Mockito.when(mockFamilyService.getFamilyById(Mockito.anyInt())).thenReturn(mockFamily);
        Mockito.when(mockFamily.countFamily()).thenReturn(1);


        Mockito.when(mockFamily.getMother()).thenReturn(mockWoman);
        Mockito.when(mockFamily.getFather()).thenReturn(mockMan);

        long date = LocalDate.of(2022, 12, 12).atStartOfDay().atZone(ZoneId.systemDefault())
                .toInstant().toEpochMilli();


        Mockito.when(mockWoman.getBirthDate()).thenReturn(date);
        Mockito.when(mockMan.getBirthDate()).thenReturn(date);

        familyController.adoptChild();

        assertEquals("The family has members younger than 18 years. It is impossible to give birth to a child",
                outputErrorStreamCaptor.toString().trim());

    }

    @Test
    public void testBornChild() {
        Mockito.when(mockFamilyService.bornChild(Mockito.any(), Mockito.anyString(), Mockito.anyString()))
                .thenReturn(mockFamily);

        Family family = familyController.bornChild(mockFamily, "Boy", "Girl");

        assertEquals(mockFamily, family);
    }

    @Test
    public void testBornChildWithFamilyIndexOutOfBoundsException() {
        Mockito.when(mockScanner.nextLine()).thenReturn("0");

        familyController.bornChild();

        assertEquals("Invalid family index",
                outputErrorStreamCaptor.toString().trim());
    }

    @Test
    public void testBornChildWithCatchFamilyOverflowException() {
        Mockito.when(mockScanner.nextLine()).thenReturn("0");

        Mockito.when(mockFamilyService.getAllFamilies()).thenReturn(new ArrayList<>(List.of(mockFamily)));

        Mockito.when(mockFamilyService.getFamilyById(Mockito.anyInt())).thenReturn(mockFamily);
        Mockito.when(mockFamily.countFamily()).thenReturn(10);

        familyController.bornChild();

        assertEquals("It is not possible to add a child to this family. Limit exceeded",
                outputErrorStreamCaptor.toString().trim());

    }

    @Test
    public void testBornChildWithFamilyImpossibleBornChildException() {
        Mockito.when(mockScanner.nextLine()).thenReturn("0");

        Mockito.when(mockFamilyService.getAllFamilies()).thenReturn(new ArrayList<>(List.of(mockFamily)));

        Mockito.when(mockFamilyService.getFamilyById(Mockito.anyInt())).thenReturn(mockFamily);
        Mockito.when(mockFamily.countFamily()).thenReturn(1);


        Mockito.when(mockFamily.getMother()).thenReturn(mockWoman);
        Mockito.when(mockFamily.getFather()).thenReturn(mockMan);

        long date = LocalDate.of(2022, 12, 12).atStartOfDay().atZone(ZoneId.systemDefault())
                .toInstant().toEpochMilli();


        Mockito.when(mockWoman.getBirthDate()).thenReturn(date);
        Mockito.when(mockMan.getBirthDate()).thenReturn(date);

        familyController.bornChild();

        assertEquals("The family has members younger than 18 years. It is impossible to give birth to a child",
                outputErrorStreamCaptor.toString().trim());

    }

    @Test
    public void testGetFamilyById() {
        try {
            Mockito.when(mockFamilyService.getAllFamilies()).thenReturn(new ArrayList<>(List.of(mockFamily)));
            Mockito.when(mockFamilyService.getFamilyById(Mockito.anyInt())).thenReturn(mockFamily);

            Family family = familyController.getFamilyById(0);

            assertEquals(mockFamily, family);
        } catch (FamilyIndexOutOfBoundsException e) {

        }
    }

    @Test
    public void testGetFamilyByIdWithFamilyIndexOutOfBoundsExceptionNegativeNumber() {
        Mockito.when(mockFamilyService.getAllFamilies()).thenReturn(new ArrayList<>());

        Exception exception = Assertions.assertThrows(FamilyIndexOutOfBoundsException.class, () -> {
            familyController.getFamilyById(-1);
        });

        assertEquals("Invalid family index", exception.getMessage());
    }

    @Test
    public void testGetFamilyByIdWithFamilyIndexOutOfBoundsExceptionPositiveNumber() {
        Mockito.when(mockFamilyService.getAllFamilies()).thenReturn(new ArrayList<>());

        Exception exception = Assertions.assertThrows(FamilyIndexOutOfBoundsException.class, () -> {
            familyController.getFamilyById(2);
        });

        assertEquals("Invalid family index", exception.getMessage());
    }

    @Test
    public void testDisplayFamiliesBiggerThanFailed() {
        Mockito.when(mockScanner.nextLine()).thenReturn("-1");

        familyController.displayFamiliesBiggerThan();

        assertEquals("Invalid quantity, please try again!!!",
                outputErrorStreamCaptor.toString().trim());
    }

    @Test
    public void testDisplayFamiliesLessThanFailed() {
        Mockito.when(mockScanner.nextLine()).thenReturn("-1");

        familyController.displayFamiliesLessThan();

        assertEquals("Invalid quantity, please try again!!!",
                outputErrorStreamCaptor.toString().trim());
    }

    @Test
    public void testDeleteFamilyByIndexFailed() {
        Mockito.when(mockScanner.nextLine()).thenReturn("-1");

        Mockito.when(mockFamilyService.getAllFamilies()).thenReturn(new ArrayList<>());

        familyController.deleteFamilyByIndex();

        assertEquals("Invalid family index", outputErrorStreamCaptor.toString().trim());
    }

    @Test
    public void testDeleteFamilyByIndex() {
        Mockito.when(mockScanner.nextLine()).thenReturn("0");

        Mockito.when(mockFamilyService.getAllFamilies()).thenReturn(new ArrayList<>(List.of(mockFamily)));

        familyController.deleteFamilyByIndex();

        Mockito.verify(mockFamilyService, Mockito.times(1))
                .deleteFamilyByIndex(0);
    }

    @Test
    public void testDeleteAllChildrenOlderThenFailed() {
        Mockito.when(mockScanner.nextLine()).thenReturn("-1");

        familyController.deleteAllChildrenOlderThen();

        assertEquals("Invalid year", outputErrorStreamCaptor.toString().trim());
    }

    @Test
    public void testDeleteAllChildrenOlderThen() {
        Mockito.when(mockScanner.nextLine()).thenReturn("0");


        familyController.deleteAllChildrenOlderThen();

        Mockito.verify(mockFamilyService, Mockito.times(1))
                .deleteAllChildrenOlderThen(0);
    }
}
