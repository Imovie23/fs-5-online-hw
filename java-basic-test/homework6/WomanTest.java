package homework6;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
@DisplayName("Woman homework6")
public class WomanTest {
    private Woman womanWithoutFamily;
    private Woman womanWithFamily;
    @Mock
    private Man husband;
    @Mock
    private Woman wife;
    @Mock
    private AbstractPet petMock;
    @InjectMocks
    private Family familyMock;

    private final PrintStream standardOut = System.out;
    private final ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();

    @BeforeEach
    void setUp() {
        System.setOut(new PrintStream(outputStreamCaptor));

        womanWithoutFamily = new Woman("Marge", "Simpson", 1987, 90,
                new String[][]{
                        {DayOfWeek.MONDAY.name(), "Clean kitchen"},
                });

        womanWithFamily = new Woman("Marge", "Simpson", 1987, 90,
                new String[][]{
                        {DayOfWeek.MONDAY.name(), "Clean kitchen"},
                });

        petMock = Mockito.mock(AbstractPet.class);

        familyMock = Mockito.mock(Family.class);

        womanWithFamily.setFamily(familyMock);
    }

    @AfterEach
    public void tearDown() {
        System.setOut(standardOut);
    }

    @DisplayName("Checking the equality of the toString method")
    @Test
    public void testToString() {

        String expected = "Woman{"
                + "name='" + "Marge" + "', "
                + "surname='" + "Simpson" + "', "
                + "year=" + 1987 + ", "
                + "iq=" + 90 + ", "
                + "schedule=" + "[[MONDAY, Clean kitchen]]"
                + "}";

        assertEquals(expected, womanWithoutFamily.toString());
    }

    @DisplayName("Message checking for the makeup method without")
    @Test
    public void testMakeup() {
        womanWithoutFamily.makeup();

        assertEquals("Hello. I'm doing my makeup right now.", outputStreamCaptor.toString().trim());
    }

    @DisplayName("Checking message for the greet pet method with family")
    @Test
    public void testGreetPet() {
        Mockito.when(familyMock.getPet()).thenReturn(petMock);
        Mockito.when(petMock.getNickname()).thenReturn("Santa's Little Helper");

        womanWithFamily.greetPet();

        String expectedMassage = "Hello, Santa's Little Helper. Did you go to sleep?";

        assertEquals(expectedMassage, outputStreamCaptor.toString().trim());
    }

    @DisplayName("Error checking for the greet pet method without family")
    @Test
    public void testGreetPetFailed() {
        womanWithoutFamily.greetPet();

        assertEquals("There is no pet in the family!!!", outputStreamCaptor.toString().trim());
    }

}
