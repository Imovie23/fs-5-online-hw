package homework6;

import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
@DisplayName("Human homework6")
public class HumanTest {
    private Human humanWithoutFamily;
    private Human humanWithFamily;
    @Mock
    private Man man;
    @Mock
    private Woman woman;
    @Mock
    private AbstractPet petMock;
    @InjectMocks
    private Family familyMock;


    private final PrintStream standardOut = System.out;
    private final ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();
    private final String ERROR_MESSAGE = "There is no pet in the family!!!";

    @BeforeEach
    void setUp() {
        System.setOut(new PrintStream(outputStreamCaptor));

        humanWithoutFamily = new Human("Homer", "Simpson", 1987, 55,
                new String[][]{
                        {DayOfWeek.MONDAY.name(), "Drink beer"},
                        {DayOfWeek.FRIDAY.name(), "Drink beer"}
                });

        humanWithFamily = new Human("Homer", "Simpson", 1987, 55,
                new String[][]{
                        {DayOfWeek.MONDAY.name(), "Drink beer"},
                        {DayOfWeek.FRIDAY.name(), "Drink beer"}
                });

        petMock = Mockito.mock(AbstractPet.class);

        familyMock = Mockito.mock(Family.class);

        humanWithFamily.setFamily(familyMock);
    }

    @AfterEach
    public void tearDown() {
        System.setOut(standardOut);
    }

    @DisplayName("Checking the equality of the toString method")
    @Test
    public void testToString() {

        String expected = "Human{"
                + "name='" + "Homer" + "', "
                + "surname='" + "Simpson" + "', "
                + "year=" + 1987 + ", "
                + "iq=" + 55 + ", "
                + "schedule=" + "[[MONDAY, Drink beer], [FRIDAY, Drink beer]]"
                + "}";

        assertEquals(expected, humanWithoutFamily.toString());
    }

    @DisplayName("Checking message for the greet pet method with family")
    @Test
    public void testGreetPet() {
        Mockito.when(familyMock.getPet()).thenReturn(petMock);
        Mockito.when(petMock.getNickname()).thenReturn("Santa's Little Helper");

        humanWithFamily.greetPet();

        String expectedMassage = "Привіт, Santa's Little Helper";

        assertEquals(expectedMassage, outputStreamCaptor.toString().trim());
    }

    @DisplayName("Error checking for the greet pet method without family")
    @Test
    public void testGreetPetFailed() {
        humanWithoutFamily.greetPet();

        assertEquals(ERROR_MESSAGE, outputStreamCaptor.toString().trim());
    }

    @DisplayName("Checking message for the describe with trick level less then 50 pet method with family")
    @Test
    public void testDescribePetTrickLevelLessThenFifty() {
        Mockito.when(familyMock.getPet()).thenReturn(petMock);
        Mockito.when(petMock.getSpecies()).thenReturn(Species.DOG);
        Mockito.when(petMock.getAge()).thenReturn(10);
        Mockito.when(petMock.getTrickLevel()).thenReturn(40);

        humanWithFamily.describePet();

        String expectedMassage = String.format("У мене є %s, їй %d років, він %s",
                Species.DOG, 10, "майже не хитрий");

        assertEquals(expectedMassage, outputStreamCaptor.toString().trim());
    }

    @DisplayName("Checking message for the describe with trick level greater then 50 pet method with family")
    @Test
    public void testDescribePetTrickLevelGreaterThenFifty() {
        Mockito.when(familyMock.getPet()).thenReturn(petMock);
        Mockito.when(petMock.getSpecies()).thenReturn(Species.DOG);
        Mockito.when(petMock.getAge()).thenReturn(10);
        Mockito.when(petMock.getTrickLevel()).thenReturn(55);

        humanWithFamily.describePet();

        String expectedMassage = String.format("У мене є %s, їй %d років, він %s",
                Species.DOG, 10, "дуже хитрий");

        assertEquals(expectedMassage, outputStreamCaptor.toString().trim());
    }

    @DisplayName("Error checking for the describe pet method without family")
    @Test
    public void testDescribePetFailed() {
        humanWithoutFamily.describePet();

        assertEquals(ERROR_MESSAGE, outputStreamCaptor.toString().trim());
    }

    @DisplayName("Error checking for the feed pet method without family")
    @Test
    public void testFeedPetFailed() {
        humanWithoutFamily.describePet();

        assertEquals(ERROR_MESSAGE, outputStreamCaptor.toString().trim());
    }

    @DisplayName("Checking the family has pet method without a family")
    @Test
    public void testFamilyHasPetWithoutFamily() {
        boolean hasPet = humanWithoutFamily.familyHasPet();

        Assertions.assertFalse(hasPet);
    }

    @DisplayName("Checking the family has pet method with a family")
    @Test
    public void testFamilyHasPetWithFamily() {
        Mockito.when(familyMock.getPet()).thenReturn(petMock);
        boolean hasPet = humanWithFamily.familyHasPet();

        Assertions.assertTrue(hasPet);
    }

    @DisplayName("Checking the message of notify pet message method")
    @Test
    public void testNotifyPetMessage() {
        humanWithFamily.notifyPetMessage();
        String expectedMessage = "There is no pet in the family!!!";

        assertEquals(expectedMessage, outputStreamCaptor.toString().trim());
    }


}
