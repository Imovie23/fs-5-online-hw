package homework6;

import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
@DisplayName("Man homework6")
public class ManTest {
    private Man manWithoutFamily;
    private Man manWithFamily;
    @Mock
    private Man husband;
    @Mock
    private Woman wife;
    @Mock
    private AbstractPet petMock;
    @InjectMocks
    private Family familyMock;

    private final PrintStream standardOut = System.out;
    private final ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();

    @BeforeEach
    void setUp() {
        System.setOut(new PrintStream(outputStreamCaptor));

        manWithoutFamily = new Man("Homer", "Simpson", 1987, 55,
                new String[][]{
                        {DayOfWeek.MONDAY.name(), "Drink beer"},
                        {DayOfWeek.FRIDAY.name(), "Drink beer"}
                });

        manWithFamily = new Man("Homer", "Simpson", 1987, 55,
                new String[][]{
                        {DayOfWeek.MONDAY.name(), "Drink beer"},
                        {DayOfWeek.FRIDAY.name(), "Drink beer"}
                });

        petMock = Mockito.mock(AbstractPet.class);

        familyMock = Mockito.mock(Family.class);

        manWithFamily.setFamily(familyMock);
    }

    @AfterEach
    public void tearDown() {
        System.setOut(standardOut);
    }

    @DisplayName("Checking the equality of the toString method")
    @Test
    public void testToString() {

        String expected = "Man{"
                + "name='" + "Homer" + "', "
                + "surname='" + "Simpson" + "', "
                + "year=" + 1987 + ", "
                + "iq=" + 55 + ", "
                + "schedule=" + "[[MONDAY, Drink beer], [FRIDAY, Drink beer]]"
                + "}";

        assertEquals(expected, manWithoutFamily.toString());
    }

    @DisplayName("Message checking for the repair car method without")
    @Test
    public void testRepairCar() {
        manWithoutFamily.repairCar();

        assertEquals("Hello. I'm fixing the car now.", outputStreamCaptor.toString().trim());
    }

    @DisplayName("Checking message for the greet pet method with family")
    @Test
    public void testGreetPet() {
        Mockito.when(familyMock.getPet()).thenReturn(petMock);
        Mockito.when(petMock.getNickname()).thenReturn("Santa's Little Helper");

        manWithFamily.greetPet();

        String expectedMassage = "Hello, Santa's Little Helper. Did you go for a walk?";

        assertEquals(expectedMassage, outputStreamCaptor.toString().trim());
    }

    @DisplayName("Error checking for the greet pet method without family")
    @Test
    public void testGreetPetFailed() {
        manWithoutFamily.greetPet();

        assertEquals("There is no pet in the family!!!", outputStreamCaptor.toString().trim());
    }

}
