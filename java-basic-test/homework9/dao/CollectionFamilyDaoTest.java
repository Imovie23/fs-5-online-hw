package homework9.dao;

import homework9.model.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
public class CollectionFamilyDaoTest {
    private CollectionFamilyDao familyDao;
    @Mock
    private Man man;
    @Mock
    Woman woman;
    @InjectMocks
    Family mockFamily;

    @BeforeEach
    void setUp() {
        this.familyDao = new CollectionFamilyDao();
    }

    @Test
    public void testGetAllFamilies() {
        List<Family> families = familyDao.getAllFamilies();

        assertEquals(0, families.size());
    }

    @Test
    public void testGetAllFamiliesWithAdd() {
        familyDao.saveFamily(mockFamily);

        List<Family> families = familyDao.getAllFamilies();

        assertEquals(1, families.size());
    }

    @Test
    public void testSaveFamilyAddFamily() {
        familyDao.saveFamily(mockFamily);

        List<Family> families = familyDao.getAllFamilies();

        assertEquals(1, families.size());
    }

    @Test
    public void testSaveFamilyUpdateFamily() {
        familyDao.saveFamily(mockFamily);
        familyDao.saveFamily(mockFamily);

        List<Family> families = familyDao.getAllFamilies();

        assertEquals(1, families.size());
    }

    @Test
    public void testDeleteFamilyByIndex() {
        familyDao.saveFamily(mockFamily);

        List<Family> families = familyDao.getAllFamilies();

        assertEquals(1, families.size());

        boolean expected = familyDao.deleteFamily(0);

        assertEquals(0, families.size());
        Assertions.assertTrue(expected);
    }

    @Test
    public void testDeleteFamilyByIndexWrongIndex() {
        Family outCurrentFamily = Mockito.mock(Family.class);

        familyDao.saveFamily(mockFamily);
        familyDao.saveFamily(outCurrentFamily);

        List<Family> families = familyDao.getAllFamilies();

        assertEquals(2, families.size());

        boolean expected = familyDao.deleteFamily(4);

        assertEquals(2, families.size());
        Assertions.assertFalse(expected);
    }

    @Test
    public void testDeleteFamilyByFamily() {
        familyDao.saveFamily(mockFamily);

        List<Family> families = familyDao.getAllFamilies();

        assertEquals(1, families.size());

        boolean expected = familyDao.deleteFamily(mockFamily);

        assertEquals(0, families.size());
        Assertions.assertTrue(expected);
    }

    @Test
    public void testDeleteFamilyByFamilyWithWrongFamily() {
        familyDao.saveFamily(mockFamily);

        List<Family> families = familyDao.getAllFamilies();

        assertEquals(1, families.size());

        Family outCurrentFamily = Mockito.mock(Family.class);

        boolean expected = familyDao.deleteFamily(outCurrentFamily);

        assertEquals(1, families.size());
        Assertions.assertFalse(expected);
    }

    @Test
    public void testGetFamilyByIndex() {
        familyDao.saveFamily(mockFamily);

        assertEquals(mockFamily, familyDao.getFamilyByIndex(0));
    }

    @Test
    public void testGetFamilyByIndexWithWrongIndex() {
        familyDao.saveFamily(mockFamily);

        Assertions.assertNull(familyDao.getFamilyByIndex(1));
    }

}
