package homework9.service;

import homework9.dao.CollectionFamilyDao;
import homework9.model.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
public class DefaultFamilyServiceTest {
    @Mock
    private CollectionFamilyDao familyDao;
    @Mock
    private Family mockFamily;
    @InjectMocks
    private DefaultFamilyService familyService;
    @Mock
    private Human mockMan;
    @Mock
    private Human mockWoman;
    @Mock
    private AbstractPet mockPet;
    @Mock
    private Man mockBoy;
    @Mock
    private Woman mockGirl;
    private final PrintStream standardOut = System.out;
    private final ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();
    private final String EXPECTED_FAMILY = "Family{"
            + "mother=" + "MockMan" + ", "
            + "father=" + "MockMan" + ", "
            + "children=" + "[]" + ", "
            + "pet=" + null
            + "}";
    private final String EXPECTED_FAMILY_LIST = "[" + EXPECTED_FAMILY + "]";

    @BeforeEach
    void setUp() {
        System.setOut(new PrintStream(outputStreamCaptor));
    }

    @AfterEach
    public void tearDown() {
        System.setOut(standardOut);
    }

    @Test
    public void testAdoptChild() {
        mockFamily.addChild(mockMan);

        Family familyWithChild = familyService.adoptChild(mockFamily, mockMan);

        assertEquals(mockFamily, familyWithChild);
    }

    @Test
    public void testAdoptChildWithNullFamily() {
        mockFamily.addChild(mockMan);

        Family familyWithChild = familyService.adoptChild(null, mockMan);

        Assertions.assertNull(familyWithChild);
    }

    @Test
    public void testAdoptChildWithNullChild() {
        mockFamily.addChild(mockMan);

        Family familyWithChild = familyService.adoptChild(mockFamily, null);

        Assertions.assertNull(familyWithChild);
    }

    @Test
    public void testBornChildBoy() {
        Mockito.when(mockFamily.bornChild()).thenReturn(mockBoy);

        familyService.bornChild(mockFamily, "Boy", "Girl");

        Mockito.verify(mockBoy, Mockito.times(1))
                .setName("Boy");

    }

    @Test
    public void testBornChildGirl() {
        Mockito.when(mockFamily.bornChild()).thenReturn(mockGirl);

        familyService.bornChild(mockFamily, "Boy", "Girl");

        Mockito.verify(mockGirl, Mockito.times(1))
                .setName("Girl");
    }

    @Test
    public void testBornChildNull() {
        Family family = familyService.bornChild(null, "Boy", "Girl");

        Assertions.assertNull(family);
    }

    @Test
    public void testGetFamiliesBiggerThan() {
        Mockito.when(mockFamily.toString()).thenReturn(EXPECTED_FAMILY);
        Mockito.when(familyDao.getAllFamilies()).thenReturn(new ArrayList<>(List.of(mockFamily, mockFamily)));
        Mockito.when(mockFamily.countFamily()).thenReturn(2);

        familyService.getFamiliesBiggerThan(1);

        String expected = "[" + EXPECTED_FAMILY + ", " + EXPECTED_FAMILY + "]";

        assertEquals(expected, outputStreamCaptor.toString().trim());
    }

    @Test
    public void testGetFamiliesBiggerThanEmpty() {
        Mockito.when(familyDao.getAllFamilies()).thenReturn(new ArrayList<>(List.of(mockFamily, mockFamily)));
        Mockito.when(mockFamily.countFamily()).thenReturn(2);

        familyService.getFamiliesBiggerThan(2);

        String expected = "[]";

        assertEquals(expected, outputStreamCaptor.toString().trim());
    }

    @Test
    public void testGetFamiliesLessThan() {
        Mockito.when(mockFamily.toString()).thenReturn(EXPECTED_FAMILY);
        Mockito.when(familyDao.getAllFamilies()).thenReturn(new ArrayList<>(List.of(mockFamily, mockFamily)));
        Mockito.when(mockFamily.countFamily()).thenReturn(2);

        familyService.getFamiliesLessThan(3);

        String expected = "[" + EXPECTED_FAMILY + ", " + EXPECTED_FAMILY + "]";

        assertEquals(expected, outputStreamCaptor.toString().trim());
    }

    @Test
    public void testGetFamiliesLessThanEmpty() {
        Mockito.when(familyDao.getAllFamilies()).thenReturn(new ArrayList<>(List.of(mockFamily, mockFamily)));
        Mockito.when(mockFamily.countFamily()).thenReturn(2);

        familyService.getFamiliesLessThan(1);

        String expected = "[]";

        assertEquals(expected, outputStreamCaptor.toString().trim());
    }

    @Test
    public void testCountFamiliesWithMemberNumber() {
        Mockito.when(familyDao.getAllFamilies()).thenReturn(new ArrayList<>(List.of(mockFamily, mockFamily)));
        Mockito.when(mockFamily.countFamily()).thenReturn(2);

        int actual = familyService.countFamiliesWithMemberNumber(2);

        assertEquals(2, actual);
    }

    @Test
    public void testCountFamiliesWithMemberNumberEmpty() {
        Mockito.when(familyDao.getAllFamilies()).thenReturn(new ArrayList<>(List.of(mockFamily, mockFamily)));
        Mockito.when(mockFamily.countFamily()).thenReturn(2);

        int actual = familyService.countFamiliesWithMemberNumber(3);

        assertEquals(0, actual);
    }

    @Test
    public void testCount() {
        Mockito.when(familyDao.getAllFamilies()).thenReturn(new ArrayList<>(List.of(mockFamily, mockFamily)));

        int expected = familyService.count();

        assertEquals(expected, 2);
    }

    @Test
    public void testDisplayAllFamiliesEmpty() {
        familyService.displayAllFamilies();

        assertEquals("[]", outputStreamCaptor.toString().trim());
    }

    @Test
    public void testDisplayAllFamiliesNotEmpty() {
        Mockito.when(mockFamily.toString()).thenReturn(EXPECTED_FAMILY);
        Mockito.when(familyDao.getAllFamilies()).thenReturn(new ArrayList<>(List.of(mockFamily)));


        familyService.displayAllFamilies();

        String expected = "№1: " + EXPECTED_FAMILY;

        assertEquals(expected, outputStreamCaptor.toString().trim());
    }

    @Test
    public void testGetAllFamiliesEmpty() {
        List<Family> families = familyService.getAllFamilies();

        assertEquals(0, families.size());
    }

    @Test
    public void testGetAllFamiliesNotEmpty() {
        Mockito.when(familyDao.getAllFamilies()).thenReturn(new ArrayList<>(List.of(mockFamily, mockFamily)));

        List<Family> families = familyService.getAllFamilies();

        assertEquals(2, families.size());
    }

    @Test
    public void testDeleteAllChildrenOlderThenAllDeleted() {
        Mockito.when(familyDao.getAllFamilies()).thenReturn(new ArrayList<>(List.of(mockFamily, mockFamily)));
        Mockito.when(mockFamily.getChildren()).thenReturn(new ArrayList<>(List.of(mockMan, mockWoman)));

        Mockito.when(mockMan.getBirthDate()).thenReturn(System.currentTimeMillis() - 315_576_000_000L);
        Mockito.when(mockWoman.getBirthDate()).thenReturn(System.currentTimeMillis() - 568_036_800_000L);

        familyService.deleteAllChildrenOlderThen(5);


        Mockito.verify(familyDao, Mockito.times(4))
                .deleteFamily(mockFamily);

        Mockito.verify(mockFamily, Mockito.times(2))
                .deleteChild(mockMan);
        Mockito.verify(mockFamily, Mockito.times(2))
                .deleteChild(mockWoman);

        Mockito.verify(familyDao, Mockito.times(2))
                .saveFamily(mockFamily);
    }

    @Test
    public void testDeleteAllChildrenOlderThenOneOfChildDeleted() {
        Mockito.when(familyDao.getAllFamilies()).thenReturn(new ArrayList<>(List.of(mockFamily, mockFamily)));
        Mockito.when(mockFamily.getChildren()).thenReturn(new ArrayList<>(List.of(mockMan, mockWoman)));

        Mockito.when(mockMan.getBirthDate()).thenReturn(System.currentTimeMillis() - 315_576_000_000L);
        Mockito.when(mockWoman.getBirthDate()).thenReturn(System.currentTimeMillis() - 568_036_800_000L);

        familyService.deleteAllChildrenOlderThen(10);


        Mockito.verify(familyDao, Mockito.times(2))
                .deleteFamily(mockFamily);

        Mockito.verify(mockFamily, Mockito.times(0))
                .deleteChild(mockMan);

        Mockito.verify(mockFamily, Mockito.times(2))
                .deleteChild(mockWoman);

        Mockito.verify(familyDao, Mockito.times(2))
                .saveFamily(mockFamily);

    }

    @Test
    public void testGetPets() {
        Mockito.when(familyDao.getFamilyByIndex(Mockito.anyInt())).thenReturn(mockFamily);
        Mockito.when(mockFamily.getPets()).thenReturn(new ArrayList<>(List.of(mockPet)));

        List<AbstractPet> pets = familyService.getPets(0);

        assertEquals(1, pets.size());
    }

    @Test
    public void testAddPet() {
        Mockito.when(familyDao.getFamilyByIndex(Mockito.anyInt())).thenReturn(mockFamily);

        familyService.addPets(0, mockPet);

        Mockito.verify(mockFamily, Mockito.times(1))
                .addPet(mockPet);

        Mockito.verify(familyDao, Mockito.times(1))
                .saveFamily(mockFamily);
    }

    @Test
    public void testAddPetWithWrongIndex() {
        familyService.addPets(1, mockPet);

        Mockito.verify(mockFamily, Mockito.times(0))
                .addPet(mockPet);

        Mockito.verify(familyDao, Mockito.times(0))
                .saveFamily(mockFamily);
    }

    @Test
    public void testGetFamilyById() {
        Mockito.when(familyDao.getFamilyByIndex(Mockito.anyInt())).thenReturn(mockFamily);

        Family family = familyService.getFamilyById(1);

        assertEquals(mockFamily, family);
    }

    @Test
    public void testDeleteFamilyByIndex() {
        familyService.deleteFamilyByIndex(0);

        Mockito.verify(familyDao, Mockito.times(1))
                .deleteFamily(0);
    }

    @Test
    public void testCreateNewFamily() {
        familyService.createNewFamily(mockWoman, mockMan);

        Mockito.verify(familyDao, Mockito.times(1))
                .saveFamily(new Family(mockWoman, mockMan));
    }
}
