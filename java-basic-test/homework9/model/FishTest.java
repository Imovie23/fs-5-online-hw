package homework9.model;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.HashSet;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DisplayName("Fish homework9")
public class FishTest {
    private Fish fish;
    private final PrintStream standardOut = System.out;
    private final ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();

    @BeforeEach
    void setUp() {
        System.setOut(new PrintStream(outputStreamCaptor));

        fish = new Fish("Blinky", 99, 100,
                new HashSet<>(List.of( "Swim")));
    }

    @AfterEach
    public void tearDown() {
        System.setOut(standardOut);
    }

    @DisplayName("Checking the equality of the toString method")
    @Test
    public void testToString() {

        String expected = Species.FISH + "{"
                + "nickname='" + "Blinky" + "', "
                + "age=" + 99 + ", "
                + "trickLevel=" + 100 + ", "
                + "habits=" + "[Swim]"
                + "}";

        assertEquals(expected, fish.toString());
    }

    @DisplayName("Checking for the eat method")
    @Test
    public void testEat() {
        fish.eat();
        String expectedMessage = "I'm eating!";

        assertEquals(expectedMessage, outputStreamCaptor.toString().trim());
    }

    @DisplayName("Checking for the respond method")
    @Test
    public void testRespond() {
        fish.respond();
        String expectedMessage = String.format("Hello, host. I am a %s named %s.",
                fish.getSpecies(), fish.getNickname());

        assertEquals(expectedMessage, outputStreamCaptor.toString().trim());
    }

}
