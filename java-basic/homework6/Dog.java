package homework6;

public class Dog extends AbstractPet implements PetFoul {
    public Dog() {
    }

    public Dog(String nickname) {
        super(nickname);
    }

    public Dog(String nickname, int age, int trickLevel, String[] habits) {
        super(nickname, age, trickLevel, habits);
    }

    @Override
    public void respond() {
        System.out.printf("Hello, host. I am a %s named %s.\n", getSpecies(), getNickname());
    }

    @Override
    public void foul() {
        System.out.println("You need to cover your tracks well...");
    }
}
