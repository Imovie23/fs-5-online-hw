package homework6;

public class HappyFamilyMain {

    public static void main(String[] args) {
        Dog santaLittleHelpers = new Dog("Santa's Little Helper", 10, 50, new String[]{"Sleep"});
        santaLittleHelpers.setSpecies(Species.DOG);

        DomesticCat SnowballV = new DomesticCat("Snowball V", 5, 13, new String[]{"Sleep"});

        Man homer = new Man("Homer", "Simpson", 1987, 55, new String[][]{{DayOfWeek.MONDAY.name(), "Drink beer"}, {DayOfWeek.FRIDAY.name(), "Drink beer"}});
        Woman marge = new Woman("Marge", "Simpson", 1987, 90, new String[][]{{DayOfWeek.MONDAY.name(), "Clean kitchen"}});

        Family simpsons = new Family(marge, homer);

        simpsons.setPet(santaLittleHelpers);

        Human child = simpsons.bornChild();

        System.out.printf("A newborn baby: %s \n\n", child);
        System.out.printf("Father's family: %s \n\n", homer.getFamily());
        System.out.printf("Family of a newborn baby: %s \n\n",  child.getFamily());

        simpsons.setPet(SnowballV);

        System.out.printf("A family of a newborn baby with an animal of an unknown species: %s \n\n",  child.getFamily());
    }


}
