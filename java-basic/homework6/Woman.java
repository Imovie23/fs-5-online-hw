package homework6;

import java.util.Arrays;

final public class Woman extends Human {
    public Woman() {
    }

    public Woman(String name) {
        super(name);
    }

    public Woman(String name, String surname, int year) {
        super(name, surname, year);
    }

    public Woman(String name, String surname, int year, int iq, String[][] schedule) {
        super(name, surname, year, iq, schedule);
    }

    public void makeup() {
        System.out.println("Hello. I'm doing my makeup right now.");
    }

    @Override
    public void greetPet() {
        if (familyHasPet()) {
            System.out.printf("Hello, %s. Did you go to sleep?", getFamily().getPet().getNickname());
            return;
        }

        notifyPetMessage();
    }

    @Override
    public String toString() {
        return "Woman{"
                + "name='" + getName() + "', "
                + "surname='" + getSurname() + "', "
                + "year=" + getYear() + ", "
                + "iq=" + getIq() + ", "
                + "schedule=" + Arrays.deepToString(getSchedule())
                + "}";
    }
}
