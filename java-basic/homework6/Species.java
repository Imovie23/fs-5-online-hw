package homework6;

public enum Species {
    ROBO_CAT, DOG, FISH, DOMESTIC_CAT, UNKNOWN
}
