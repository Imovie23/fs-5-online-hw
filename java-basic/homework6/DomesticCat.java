package homework6;

public class DomesticCat extends AbstractPet implements PetFoul {
    public DomesticCat() {
    }

    public DomesticCat(String nickname) {
        super(nickname);
    }

    public DomesticCat(String nickname, int age, int trickLevel, String[] habits) {
        super(nickname, age, trickLevel, habits);
    }

    @Override
    public void respond() {
        System.out.printf("Hello, host. I am a %s named %s.\n", getSpecies(), getNickname());
    }

    @Override
    public void foul() {
        System.out.println("Need to escape...");
    }
}
