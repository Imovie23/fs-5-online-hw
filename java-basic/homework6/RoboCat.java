package homework6;

public class RoboCat extends AbstractPet{
    public RoboCat() {
    }

    public RoboCat(String nickname) {
        super(nickname);
    }

    public RoboCat(String nickname, int age, int trickLevel, String[] habits) {
        super(nickname, age, trickLevel, habits);
    }

    @Override
    public void respond() {
        System.out.printf("Hello, host. I am a %s named %s.\n", getSpecies(), getNickname());
    }
}
