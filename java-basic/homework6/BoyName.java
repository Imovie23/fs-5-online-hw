package homework6;

public enum BoyName {
    MISHA("Misha"), PASHA("Pasha"), SASHA("Sasha"), TIKHON("Tikhon"), LUKA("Luka");
    private final String boyName;

    private BoyName(String name) {
        boyName = name;
    }

    public String getBoyName() {
        return boyName;
    }

}

