package homework6;

public class Fish extends AbstractPet {
    public Fish() {
    }

    public Fish(String nickname) {
        super(nickname);
    }

    public Fish(String nickname, int age, int trickLevel, String[] habits) {
        super(nickname, age, trickLevel, habits);
    }

    @Override
    public void respond() {
        System.out.printf("Hello, host. I am a %s named %s.\n", getSpecies(), getNickname());
    }
}
