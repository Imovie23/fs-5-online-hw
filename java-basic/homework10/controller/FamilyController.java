package homework10.controller;

import homework10.model.AbstractPet;
import homework10.model.Family;
import homework10.model.Human;
import homework10.service.FamilyService;

import java.util.List;

public class FamilyController {
    private final FamilyService familyService;

    public FamilyController(FamilyService familyService) {
        this.familyService = familyService;
    }

    public List<Family> getAllFamilies() {
        return this.familyService.getAllFamilies();
    }

    public Family adoptChild(Family family, Human child) {
        return this.familyService.adoptChild(family, child);
    }

    public Family getFamilyById(int index) {
        return this.familyService.getFamilyById(index);
    }

    public Family bornChild(Family family, String boyName, String girlName) {
        return this.familyService.bornChild(family, boyName, girlName);
    }

    public List<AbstractPet> getPets(int familyIndex) {
        return this.familyService.getPets(familyIndex);
    }

    public void getFamiliesBiggerThan(int numberPeopleInFamily) {
        this.familyService.getFamiliesBiggerThan(numberPeopleInFamily);
    }

    public void getFamiliesLessThan(int numberPeopleInFamily) {
        this.familyService.getFamiliesLessThan(numberPeopleInFamily);
    }

    public int count() {
        return this.familyService.count();
    }

    public int countFamiliesWithMemberNumber(int numberPeopleInFamily) {
        return this.familyService.countFamiliesWithMemberNumber(numberPeopleInFamily);
    }

    public void displayAllFamilies() {
        this.familyService.displayAllFamilies();
    }

    public void createNewFamily(Human woman, Human man) {
        this.familyService.createNewFamily(woman, man);
    }

    public void deleteFamilyByIndex(int index) {
        this.familyService.deleteFamilyByIndex(index);
    }

    public void deleteAllChildrenOlderThen(int year) {
        this.familyService.deleteAllChildrenOlderThen(year);
    }

    public void addPets(int index, AbstractPet pet) {
        this.familyService.addPets(index, pet);
    }
}
