package homework10;

import homework10.dao.*;
import homework10.model.*;
import homework10.service.*;
import homework10.controller.*;


import java.util.*;

public class HappyFamilyMain {

    public static void main(String[] args) {
        FamilyDao familyDao = new CollectionFamilyDao();
        FamilyService familyService = new DefaultFamilyService(familyDao);
        FamilyController familyController = new FamilyController(familyService);

        Dog santaLittleHelpers = new Dog("Santa's Little Helper", 10, 50, new HashSet<>(List.of("Sleep")));

        Map<String, Set<String>> homerSchedule = new HashMap<>();
        homerSchedule.put(DayOfWeek.MONDAY.name(), new HashSet<>(List.of("Drink beer")));
        homerSchedule.put(DayOfWeek.FRIDAY.name(), new HashSet<>(List.of("Drink beer")));

        Map<String, Set<String>> margeSchedule = new HashMap<>();
        margeSchedule.put(DayOfWeek.MONDAY.name(), new HashSet<>(List.of("Clean kitchen")));

        Man homer = new Man("Homer", "Simpson", "19/10/1987", 55, homerSchedule);
        Woman marge = new Woman("Marge", "Simpson", "22/01/1987", 90, margeSchedule);

        Human bart = new Human("Bart", "Simpson", "01/02/1999", 60);

        familyController.createNewFamily(marge, homer);
        familyController.createNewFamily(bart, homer);

        System.out.println("Create two family");
        familyController.displayAllFamilies();
        System.out.println();

        System.out.println("Get all families");
        List<Family> families = familyController.getAllFamilies();
        System.out.println(families);
        System.out.println();

        System.out.println("Get family by id");
        Family family = familyController.getFamilyById(0);
        System.out.println(family);
        System.out.println();


        System.out.println("Born child");
        familyController.bornChild(family, "Boy", "Girl");
        familyController.displayAllFamilies();
        System.out.println();

        System.out.println("Families bigger than 1");
        familyController.getFamiliesBiggerThan(1);
        System.out.println();

        System.out.println("Families bigger than 2");
        familyController.getFamiliesBiggerThan(2);
        System.out.println();

        System.out.println("Families less than 4");
        familyController.getFamiliesLessThan(4);
        System.out.println();

        System.out.println("Families less than 3");
        familyController.getFamiliesLessThan(3);
        System.out.println();

        System.out.println("Count families with member number 3");
        System.out.println(familyController.countFamiliesWithMemberNumber(3));
        System.out.println();

        System.out.println("Count families with member number 2");
        System.out.println(familyController.countFamiliesWithMemberNumber(2));
        System.out.println();

        System.out.println("Adopt child");
        familyController.adoptChild(family, bart);
        familyController.displayAllFamilies();
        System.out.println();

        System.out.println("Delete all children older then 15");
        familyController.deleteAllChildrenOlderThen(15);
        familyController.displayAllFamilies();
        System.out.println();

        System.out.println("Delete family by index");
        familyController.deleteFamilyByIndex(1);
        familyController.displayAllFamilies();
        System.out.println();

        System.out.println("Count");
        System.out.println(familyController.count());
        System.out.println();

        System.out.println("Get Pets");
        System.out.println(familyController.getPets(0));
        System.out.println();

        System.out.println("Add pets");
        familyController.addPets(0, santaLittleHelpers);
        System.out.println(familyController.getPets(0));
        System.out.println();

    }
}
