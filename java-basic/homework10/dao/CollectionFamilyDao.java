package homework10.dao;

import homework10.model.Family;

import java.util.ArrayList;
import java.util.List;

public class CollectionFamilyDao implements FamilyDao {

    private final List<Family> families = new ArrayList<>();

    @Override
    public List<Family> getAllFamilies() {
        return this.families;
    }

    @Override
    public Family getFamilyByIndex(int index) {
        int familyCount = this.families.size() - 1;

        if (index < 0 || index > familyCount) {
            return null;
        }

        return this.families.get(index);
    }

    @Override
    public boolean deleteFamily(int index) {
        int familyCount = this.families.size() - 1;

        if (index < 0 || index > familyCount) {
            return false;
        }

        this.families.remove(index);

        return true;
    }

    @Override
    public boolean deleteFamily(Family family) {
        if (family == null) {
            return false;
        }

        boolean isHasFamily = this.families.contains(family);

        if (!isHasFamily) {
            return false;
        }

        this.families.remove(family);

        return true;
    }

    @Override
    public void saveFamily(Family family) {
        if (family == null) {
            return;
        }

        int familyPosition = this.families.indexOf(family);

        if (familyPosition == -1) {
            this.families.add(family);
            return;
        }

        this.families.set(familyPosition, family);
    }
}
