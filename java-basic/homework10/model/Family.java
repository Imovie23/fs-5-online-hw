package homework10.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Family implements HumanCreator {
    private final Human mother;
    private final Human father;
    private List<Human> children;
    private List<AbstractPet> pets;

    public Family(Human mother, Human father) {
        assignFamily(mother);
        assignFamily(father);

        this.mother = mother;
        this.father = father;
        this.children = new ArrayList<>();

    }

    public void addChild(Human child) {
        assignFamily(child);

        children.add(child);
    }

    private void assignFamily(Human human) {
        human.setFamily(this);
    }

    public boolean deleteChild(int childNumber) {
        int numberOfChildren = children.size();

        if (childNumber > numberOfChildren || childNumber < 0) {
            return false;
        }

        Human child = children.get(childNumber);

        unAssignFamily(child);

        children.remove(child);

        return true;

    }

    public boolean deleteChild(Human child) {
        if (childBelongsThisFamily(child)) {

            boolean isHasChild = children.contains(child);

            if (isHasChild) {
                unAssignFamily(child);

                children.remove(child);
                return true;
            }
        }

        System.out.println("This child does not belong to this family");

        return false;
    }

    private boolean childBelongsThisFamily(Human child) {
        return child != null && child.getFamily() != null && child.getFamily().equals(this);
    }

    private void unAssignFamily(Human human) {
        human.setFamily(null);
    }

    public int countFamily() {
        int INITIAL_FAMILY_COUNT = 2;
        return children.size() + INITIAL_FAMILY_COUNT;
    }

    @Override
    public Human bornChild() {
        Human child = generateBoyOrGirl();
        int averageIq = (father.getIq() + mother.getIq()) / 2;

        assignFamily(child);
        child.setSurname(father.getSurname());
        child.setIq(averageIq);

        this.children.add(child);

        return child;
    }

    private Human generateBoyOrGirl() {
        int randomGenderNumber = (int) Math.round(generateRandomInRange(0, 1));

        Human child;

        if (randomGenderNumber == 0) {
            GirlName[] girls = GirlName.values();
            int length = girls.length - 1;

            int randomGirlNumber = (int) Math.round(generateRandomInRange(0, length));

            String name = girls[randomGirlNumber].getGirlName();

            child = new Woman(name);
        } else {
            BoyName[] boys = BoyName.values();
            int length = boys.length - 1;

            int randomBoysNumber = (int) Math.round(generateRandomInRange(0, length));

            String name = boys[randomBoysNumber].getBoyName();

            child = new Man(name);
        }
        return child;
    }

    private double generateRandomInRange(int start, int end) {
        return start + Math.random() * end;
    }

    public void addPet(AbstractPet pet) {
        if (pet == null) {
            return;
        }

        if (this.pets == null) {
            setPets(new ArrayList<>(List.of(pet)));
            return;
        }

        pets.add(pet);
    }

    public Human getMother() {
        return mother;
    }

    public Human getFather() {
        return father;
    }

    public List<Human> getChildren() {
        return children;
    }

    public List<AbstractPet> getPets() {
        return pets;
    }

    public void setPets(List<AbstractPet> pet) {
        this.pets = pet;
    }

    @Override
    public String toString() {
        return "Family{"
                + "mother="
                + mother + ", "
                + "father=" + father + ", "
                + "children=" + children + ", "
                + "pet=" + pets
                + "}";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Family family = (Family) o;

        return mother.equals(family.getMother())
                && father.equals(family.getFather())
                && children.equals(family.getChildren())
                && Objects.equals(pets, family.getPets());
    }

    @Override
    public int hashCode() {
        final int PRIME = 31;
        int result = mother != null ? mother.hashCode() : 0;

        result = PRIME * result + (father != null ? father.hashCode() : 0);
        result = PRIME * result + (children != null ? children.hashCode() : 0);
        result = PRIME * result + (pets != null ? pets.hashCode() : 0);

        return result;
    }
}
