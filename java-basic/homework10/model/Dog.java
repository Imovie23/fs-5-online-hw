package homework10.model;

import java.util.Set;

public class Dog extends AbstractPet implements PetFoul {
    public Dog() {
        setSpecies(Species.DOG);
    }

    public Dog(String nickname) {
        super(nickname);
        setSpecies(Species.DOG);
    }

    public Dog(String nickname, int age, int trickLevel, Set<String> habits) {
        super(nickname, age, trickLevel, habits);
        setSpecies(Species.DOG);
    }

    @Override
    public void respond() {
        System.out.printf("Hello, host. I am a %s named %s.\n", getSpecies(), getNickname());
    }

    @Override
    public void foul() {
        System.out.println("You need to cover your tracks well...");
    }
}
