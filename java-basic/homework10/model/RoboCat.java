package homework10.model;

import java.util.Set;

public class RoboCat extends AbstractPet {
    public RoboCat() {
        setSpecies(Species.ROBO_CAT);
    }

    public RoboCat(String nickname) {
        super(nickname);
        setSpecies(Species.ROBO_CAT);
    }

    public RoboCat(String nickname, int age, int trickLevel, Set<String> habits) {
        super(nickname, age, trickLevel, habits);
        setSpecies(Species.ROBO_CAT);
    }

    @Override
    public void respond() {
        System.out.printf("Hello, host. I am a %s named %s.\n", getSpecies(), getNickname());
    }
}
