package homework8.model;

import java.util.Map;
import java.util.Set;

final public class Man extends Human {
    public Man() {
    }

    public Man(String name) {
        super(name);
    }

    public Man(String name, String surname, int year) {
        super(name, surname, year);
    }

    public Man(String name, String surname, int year, int iq, Map<String, Set<String>> schedule) {
        super(name, surname, year, iq, schedule);
    }

    public void repairCar() {
        System.out.println("Hello. I'm fixing the car now.");
    }

    @Override
    public void greetPets() {
        if (familyHasPets()) {
            for (AbstractPet pet: getFamily().getPets()) {
                System.out.printf("Hello, %s. Did you go for a walk?", pet.getNickname());
            }

            return;
        }

        notifyPetMessage();
    }

    @Override
    public String toString() {
        return "Man{"
                + "name='" + getName() + "', "
                + "surname='" + getSurname() + "', "
                + "year=" + getYear() + ", "
                + "iq=" + getIq() + ", "
                + "schedule=" + getSchedule()
                + "}";
    }
}
