package homework8.model;

public enum GirlName {
    HELEN("Helen"), MASHA("MASHA"), DASHA("Dasha"), NATASHA("Natasha");
    private final String girlName;

    private GirlName(String name) {
        girlName = name;
    }

    public String getGirlName() {
        return girlName;
    }

}

