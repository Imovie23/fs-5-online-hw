package homework8.model;

public interface Pet {
    default void eat() {
        System.out.println("I'm eating!");
    }
}

interface PetFoul {
    void foul();
}
