package homework8.service;

import homework8.model.AbstractPet;
import homework8.model.Family;
import homework8.model.Human;

import java.util.List;

public interface FamilyService {
    List<Family> getAllFamilies();
    Family adoptChild(Family family, Human child);
    Family getFamilyById(int index);
    Family bornChild(Family family, String boyName, String girlName);
    List<AbstractPet> getPets(int familyIndex);
    void getFamiliesBiggerThan(int numberPeopleInFamily);
    void getFamiliesLessThan(int numberPeopleInFamily);
    int count();
    int countFamiliesWithMemberNumber(int numberPeopleInFamily);
    void displayAllFamilies();
    void createNewFamily(Human woman, Human man);
    void deleteFamilyByIndex(int index);
    void deleteAllChildrenOlderThen(int year);
    void addPets(int index, AbstractPet pet);
}
