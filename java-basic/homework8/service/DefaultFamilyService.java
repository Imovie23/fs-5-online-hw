package homework8.service;

import homework8.dao.FamilyDao;
import homework8.model.AbstractPet;
import homework8.model.Family;
import homework8.model.Human;
import homework8.model.Man;

import java.util.ArrayList;
import java.util.List;

public class DefaultFamilyService implements FamilyService {

    private final FamilyDao familyDao;

    public DefaultFamilyService(FamilyDao familyDao) {
        this.familyDao = familyDao;
    }

    @Override
    public Family adoptChild(Family family, Human child) {
        if (isEmptyFamily(family) || isEmptyChild(child)) {
            return null;
        }

        List<Family> families = getAllFamilies();

        int familyPosition = families.indexOf(family);

        family.addChild(child);

        if (familyPosition == -1) {
            this.familyDao.saveFamily(family);
            return family;
        }

        this.familyDao.deleteFamily(familyPosition);
        this.familyDao.saveFamily(family);

        return family;
    }

    private boolean isEmptyChild(Human child) {
        return child == null;
    }

    @Override
    public Family bornChild(Family family, String boyName, String girlName) {
        if (isEmptyFamily(family)) {
            return null;
        }

        Human child = family.bornChild();

        family.deleteChild(child);

        if (child.getClass().equals(Man.class)) {
            child.setName(boyName);
        } else {
            child.setName(girlName);
        }

        adoptChild(family, child);

        return family;
    }

    private boolean isEmptyFamily(Family family) {
        return family == null;
    }

    @Override
    public void getFamiliesBiggerThan(int numberPeopleInFamily) {
        if (isNumberLessThanZero(numberPeopleInFamily)) {
            return;
        }

        List<Family> families = new ArrayList<>();

        for (Family family : getAllFamilies()) {
            if (family.countFamily() > numberPeopleInFamily) {
                families.add(family);
            }
        }

        System.out.println(families);
    }

    @Override
    public void getFamiliesLessThan(int numberPeopleInFamily) {
        if (isNumberLessThanZero(numberPeopleInFamily)) {
            return;
        }

        List<Family> families = new ArrayList<>();

        for (Family family : getAllFamilies()) {
            if (family.countFamily() < numberPeopleInFamily) {
                families.add(family);
            }
        }

        System.out.println(families);
    }

    @Override
    public int countFamiliesWithMemberNumber(int numberPeopleInFamily) {
        if (isNumberLessThanZero(numberPeopleInFamily)) {
            return 0;
        }

        int countFamilies = 0;

        for (Family family : getAllFamilies()) {
            if (family.countFamily() == numberPeopleInFamily) {
                countFamilies++;
            }
        }

        return countFamilies;
    }

    @Override
    public int count() {
        return getAllFamilies().size();
    }

    @Override
    public void displayAllFamilies() {

        List<Family> families = this.getAllFamilies();

        if (families.size() == 0) {
            System.out.println(families);
            return;
        }


        for (int i = 0; i < this.getAllFamilies().size(); i++) {
            System.out.println("  №" + (i + 1) + ": " + this.getAllFamilies().get(i));
        }
    }

    @Override
    public void deleteAllChildrenOlderThen(int year) {
        if (year < 0) {
            return;
        }

        List<Family> families = getAllFamilies();

        boolean isDeletedFamily;
        Family family;
        Human child;

        for (int i = 0; i < families.size(); i++) {
            isDeletedFamily = false;
            family = families.get(i);

            for (int j = family.getChildren().size() - 1; j >= 0; j--) {
                child = family.getChildren().get(j);

                if (year < child.getYear()) {
                    this.familyDao.deleteFamily(family);
                    family.deleteChild(child);
                    isDeletedFamily = true;
                }
            }

            if (isDeletedFamily) {
                this.familyDao.saveFamily(family);
            }
        }
    }

    @Override
    public List<Family> getAllFamilies() {
        return this.familyDao.getAllFamilies();
    }

    @Override
    public List<AbstractPet> getPets(int familyIndex) {
        List<AbstractPet> emptyList = new ArrayList<>();

        if (isNumberLessThanZero(familyIndex)) {
            return emptyList;
        }

        Family family = getFamilyById(familyIndex);

        if (family == null) {
            return emptyList;
        }

        return family.getPets();
    }

    @Override
    public void addPets(int familyIndex, AbstractPet pet) {
        if (isNumberLessThanZero(familyIndex) || pet == null) {
            return;
        }

        Family family = getFamilyById(familyIndex);

        if (family == null) {
            return;
        }

        this.familyDao.deleteFamily(familyIndex);

        family.addPet(pet);

        this.familyDao.saveFamily(family);
    }

    private boolean isNumberLessThanZero(int num) {
        return num < 0;
    }

    @Override
    public Family getFamilyById(int index) {
        return this.familyDao.getFamilyByIndex(index);
    }

    @Override
    public void deleteFamilyByIndex(int index) {
        this.familyDao.deleteFamily(index);
    }

    @Override
    public void createNewFamily(Human woman, Human man) {
        Family family = new Family(woman, man);

        this.familyDao.saveFamily(family);

    }

}
