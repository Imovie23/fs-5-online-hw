package homework7;

public interface Pet {
    default void eat() {
        System.out.println("I'm eating!");
    }
}

interface PetFoul {
    void foul();
}
