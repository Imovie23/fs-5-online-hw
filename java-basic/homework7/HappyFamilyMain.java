package homework7;

import java.util.*;

public class HappyFamilyMain {

    public static void main(String[] args) {
        Dog santaLittleHelpers = new Dog("Santa's Little Helper", 10, 50, new HashSet<>(List.of("Sleep")));
        DomesticCat SnowballV = new DomesticCat("Snowball V", 5, 13, new HashSet<>(List.of("Sleep")));

        List<AbstractPet> pets = new ArrayList<>(List.of(santaLittleHelpers, SnowballV));

        Map<String, Set<String>> homerSchedule = new HashMap<>();
        homerSchedule.put(DayOfWeek.MONDAY.name(), new HashSet<>(List.of("Drink beer")));
        homerSchedule.put(DayOfWeek.FRIDAY.name(), new HashSet<>(List.of("Drink beer")));

        Map<String, Set<String>> margeSchedule = new HashMap<>();
        margeSchedule.put(DayOfWeek.MONDAY.name(), new HashSet<>(List.of("Clean kitchen")));

        Man homer = new Man("Homer", "Simpson", 1987, 55, homerSchedule);
        Woman marge = new Woman("Marge", "Simpson", 1987, 90, margeSchedule);

        Family simpsons = new Family(marge, homer);


        Human child = simpsons.bornChild();

        simpsons.setPets(pets);

        System.out.printf("A newborn baby: %s \n\n", child);
        System.out.printf("Father's family: %s \n\n", homer.getFamily());
        System.out.printf("Family of a newborn baby: %s \n\n", child.getFamily());

    }
}
