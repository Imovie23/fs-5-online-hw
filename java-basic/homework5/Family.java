package homework5;

import java.util.Arrays;

public class Family {

    private int INITIAL_FAMILY_COUNT = 2;
    private Human mother;
    private Human father;
    private Human[] children;
    private Pet pet;

    public Family(Human mother, Human father) {
        assignFamily(mother);
        assignFamily(father);

        this.mother = mother;
        this.father = father;
        this.children = new Human[]{};
    }

    public void addChild(Human child) {
        assignFamily(child);

        Human[] born = Arrays.copyOf(children, children.length + 1);
        born[born.length - 1] = child;
        children = born;
    }

    private void assignFamily(Human human) {
        human.setFamily(this);
    }

    public boolean deleteChild(int childNumber) {
        int numberOfChildren = children.length;

        if (childNumber > numberOfChildren || childNumber < 0) {
            return false;
        }

        Human[] updatedChildren = new Human[numberOfChildren - 1];

        for (int i = 0, j = 0; i < numberOfChildren - 1; i++) {
            if (i == childNumber) {
                unAssignFamily(children[i]);
                continue;
            }

            updatedChildren[j++] = children[i];
        }

        children = updatedChildren;

        return true;
    }

    public boolean deleteChild(Human child) {
        if (childBelongsThisFamily(child)) {
            int numberOfChildren = children.length;

            Human[] updatedChildren = new Human[numberOfChildren - 1];
            Human currentChild;

            for (int i = 0, j = 0; i < numberOfChildren; i++) {
                currentChild = children[i];

                if (currentChild.equals(child)) {
                    unAssignFamily(currentChild);
                    continue;
                }

                updatedChildren[j++] = currentChild;
            }
            children = updatedChildren;

            return true;
        }

        System.out.println("This child does not belong to this family");

        return false;
    }

    private void unAssignFamily(Human human) {
        human.setFamily(null);
    }

    private boolean childBelongsThisFamily(Human child) {
        return child != null && child.getFamily() != null && child.getFamily().equals(this);
    }

    public int countFamily() {
        return children.length + INITIAL_FAMILY_COUNT;
    }

    public Human getMother() {
        return mother;
    }

    public Human getFather() {
        return father;
    }

    public Human[] getChildren() {
        return children;
    }

    public Pet getPet() {
        return pet;
    }

    private boolean familyHasPet() {
        return getPet() != null;
    }

    private void notifyPetErrorMessage() {
        System.out.println("There is no pet in the family!!!");
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }

    @Override
    public String toString() {
        return "Family{"
                + "mother=" + mother + ", "
                + "father=" + father + ", "
                + "children=" + Arrays.toString(children) + ", "
                + "pet=" + pet
                + "}";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Family family = (Family) o;

        return mother.equals(family.getMother())
                && father.equals(family.getFather())
                && Arrays.deepEquals(children, family.getChildren())
                && pet.equals(family.getPet());
    }

    @Override
    public int hashCode() {
        final int PRIME = 31;
        int result = mother != null ? mother.hashCode() : 0;

        result = PRIME * result + (father != null ? father.hashCode() : 0);
        result = PRIME * result + (children != null ? Arrays.deepHashCode(children) : 0);
        result = PRIME * result + (pet != null ? pet.hashCode() : 0);

        return result;
    }

    @Override
    @SuppressWarnings( "Overrides method that is deprecated and marked for removal" )
    protected void finalize() {
        System.out.println(this);
    }
}
