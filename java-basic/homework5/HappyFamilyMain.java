package homework5;

public class HappyFamilyMain {

    public static void main(String[] args) {
        finalizeLoop();
    }

    private static void finalizeLoop() {
        for (int i = 0; i < 1000000; i++) {
            new Human();
        }
    }


    @Override
    @SuppressWarnings("Overrides method that is deprecated and marked for removal")
    protected void finalize() {
        System.out.println(this);
    }
}
