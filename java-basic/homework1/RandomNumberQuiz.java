package homework1;

import java.util.Scanner;

public class RandomNumberQuiz {
    public static final int ENT_INTERVAL_FOR_RANDOM = 100;
    public static final int CORRECT_RANDOM_INTERVAL_NUMBER = 1;

    public static void main(String[] args) {
        System.out.println("Let the game begin!");
        System.out.println("Please enter your name:");

        Scanner scanner = new Scanner(System.in);
        String name = scanner.nextLine();
        int randomNumber;
        int guessedNumber;

        System.out.println("Please guess and enter a number from 0 to 100:");

        while (true) {
            randomNumber = (int) (Math.random() * (ENT_INTERVAL_FOR_RANDOM + CORRECT_RANDOM_INTERVAL_NUMBER));

            guessedNumber = scanner.nextInt();

            if (guessedNumber < randomNumber) {
                System.out.println("Your number is too small. Please, try again.");
                continue;
            }

            if (guessedNumber > randomNumber) {
                System.out.println("Your number is too big. Please, try again.");
                continue;
            }

            System.out.printf("Congratulations, %s!\n", name);
            scanner.close();
            break;
        }
    }
}







