package homework1;

import java.util.Scanner;
import java.util.Arrays;

public class RandomNumberQuizWithOptionalTask {
    public static final int EMPTY_NAME_LENGTH = 0;
    public static final int EVENT_YEAR_INDEX = 0;
    public static final int EVENT_QUESTION_INDEX = 1;
    public static final int CORRECT_RANDOM_INTERVAL_NUMBER = 1;

    public static void main(String[] args) {
        System.out.println("Let the game begin!");
        System.out.println("Please enter your name:");

        Scanner scanner = new Scanner(System.in);

        String name;
        String[][] worldEvents = {
                {"1989", "When the Berlin Wall fell?"},
                {"1963", "When John Kennedy was killed?"},
                {"1914", "When did the World War I began?"},
                {"1917", "When did the World War II began?"},
                {"2020", "When the COVID-19 pandemic began?"},
                {"1941", "When did the Pearl Harbour began?"},
                {"1945", "When the atomic bomb was dropped on Hiroshima?"}
        };

        int randomNumber;
        int guessedNumber;
        int eventYear;
        int endIntervalForRandom = worldEvents.length - 1;
        int[] score = {};

        while (true) {
            name = scanner.nextLine();

            if (name.length() == EMPTY_NAME_LENGTH) {
                System.err.println("Your name cannot be empty, please try again!");
                continue;
            }

            break;
        }

        while (true) {
            randomNumber = (int) (Math.random() * (endIntervalForRandom + CORRECT_RANDOM_INTERVAL_NUMBER));

            System.out.println(worldEvents[randomNumber][EVENT_QUESTION_INDEX]);

            eventYear = Integer.parseInt(worldEvents[randomNumber][EVENT_YEAR_INDEX]);

            while (true) {
                if (!scanner.hasNextInt()) {
                    System.err.println("You entered the wrong year, please try again!");
                    scanner.next();
                    continue;
                }

                guessedNumber = scanner.nextInt();

                break;
            }

            score = Arrays.copyOf(score, score.length + 1);
            score[score.length - 1] = guessedNumber;

            if (guessedNumber < eventYear) {
                System.out.println("The year you specified is too small. Please, try another question.");
                continue;
            }

            if (guessedNumber > eventYear) {
                System.out.println("The year you specified is too big. Please, try another question.");
                continue;
            }


            System.out.printf("Congratulations, %s!\n", name);
            scanner.close();
            break;
        }

        int temp;

        for (int i = 0; i < score.length; i++) {
            for (int j = 1; j < (score.length - i); j++) {
                if (score[j - 1] > score[j]) {
                    temp = score[j - 1];
                    score[j - 1] = score[j];
                    score[j] = temp;
                }
            }
        }

        System.out.printf("Your numbers: %s\n", Arrays.toString(score));

    }
}




