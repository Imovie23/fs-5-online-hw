package homework2;

import java.util.Scanner;

public class SquareShotGame {
    public static final int CORRECT_SIDE_FOR_SHOW_NUMBERS = 1;
    public static final int SIDE_OF_SQUARE = 5;
    public static final String SEPARATOR = " |";
    public static final String INITIAL_RENDER_CELL = " -" + SEPARATOR;
    public static final String MISSING_RENDER_CELL = " *" + SEPARATOR;
    public static final String SHOT_RENDER_CELL = " x" + SEPARATOR;

    public static void main(String[] args) {
        String[][] square = new String[SIDE_OF_SQUARE + CORRECT_SIDE_FOR_SHOW_NUMBERS]
                [SIDE_OF_SQUARE + CORRECT_SIDE_FOR_SHOW_NUMBERS];
        int selectedVerticalCell = 1 + (int) (Math.random() * (SIDE_OF_SQUARE));
        int selectedHorizontalCell = 1 + (int) (Math.random() * (SIDE_OF_SQUARE));

        for (int row = 0; row < square.length; row++) {
            for (int col = 0; col < square.length; col++) {
                if (row == 0) {
                    square[row][col] = " " + col + SEPARATOR;
                    continue;
                }

                if (col == 0) {
                    square[row][col] = " " + row + SEPARATOR;
                    continue;
                }

                square[row][col] = INITIAL_RENDER_CELL;
            }
        }

        Scanner scanner = new Scanner(System.in);
        int currentVerticalCell;
        int currentHorizontalCell;

        System.out.println("All set. Get ready to rumble!\n");

        while (true) {
            for (String[] strings : square) {
                System.out.println(String.join("", strings));
            }

            System.out.println("\nPlease select the vertical cell for the shot:");

            while (true) {
                if (!scanner.hasNextInt()) {
                    System.err.println("You entered the wrong number, try again. Please select a range from 1 to 5!");
                    scanner.next();
                    continue;
                }

                break;
            }


            while (true) {
                currentVerticalCell = scanner.nextInt();

                if (currentVerticalCell < 0 || currentVerticalCell > 5) {
                    System.err.println("Your selection is out of shot range. Please select a range from 1 to 5!");
                    continue;
                }

                break;
            }

            System.out.println("Please select the horizontal cell for the shot:");


            while (true) {
                currentHorizontalCell = scanner.nextInt();

                if (currentHorizontalCell < 0 || currentHorizontalCell > 5) {
                    System.err.println("Your selection is out of shot range. Please select a range from 1 to 5!");
                    continue;
                }

                break;
            }

            if (MISSING_RENDER_CELL.equals(square[currentHorizontalCell][currentVerticalCell])) {
                System.err.println("You have already selected this goal, please select another cell!\n");
                continue;
            }

            if (selectedVerticalCell != currentVerticalCell || selectedHorizontalCell != currentHorizontalCell) {
                System.err.println("You missed, try again!\n");
                square[currentHorizontalCell][currentVerticalCell] = MISSING_RENDER_CELL;
                continue;
            }

            square[currentHorizontalCell][currentVerticalCell] = SHOT_RENDER_CELL;

            break;
        }

        System.out.println("You have won!");

        for (String[] strings : square) {
            System.out.println(String.join("", strings));
        }
    }
}

