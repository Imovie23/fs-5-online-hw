package homework2;

import java.util.Scanner;

public class SquareShotGameWithOptionalTask {
    public static final int CORRECT_SIDE_FOR_SHOW_NUMBERS = 1;
    public static final int SIDE_OF_SQUARE = 5;
    public static final int RANG_OF_CELL = 3;
    public static final int VERTICAL_INDEX = 0;
    public static final int HORIZONTAL_INDEX = 1;
    public static final String SEPARATOR = " |";
    public static final String INITIAL_RENDER_CELL = " -" + SEPARATOR;
    public static final String MISSING_RENDER_CELL = " *" + SEPARATOR;
    public static final String SHOT_RENDER_CELL = " x" + SEPARATOR;
    public static final String VERTICAL_DIRECTION = "Vertical";
    public static final String HORIZONTAL_DIRECTION = "Horizontal";

    public static void main(String[] args) {
        String[] directions = {VERTICAL_DIRECTION, HORIZONTAL_DIRECTION};
        String[][] square = new String[SIDE_OF_SQUARE + CORRECT_SIDE_FOR_SHOW_NUMBERS]
                [SIDE_OF_SQUARE + CORRECT_SIDE_FOR_SHOW_NUMBERS];
        int[][] range = new int[RANG_OF_CELL][2];
        int selectedVerticalCell = 1 + (int) (Math.random() * (SIDE_OF_SQUARE));
        int selectedHorizontalCell = 1 + (int) (Math.random() * (SIDE_OF_SQUARE));
        int randomDirection = (int) (Math.random() * directions.length);
        String selectedDirection = directions[randomDirection];
        int shoots = 0;
        int halfRange = (int) Math.ceil((double) SIDE_OF_SQUARE / 2);

        for (int i = 0; i < range.length; i++) {
            for (int j = 0; j < 2; j++) {
                if (VERTICAL_DIRECTION.equals(selectedDirection)) {
                    if (j == VERTICAL_INDEX) {
                        range[i][j] = selectedVerticalCell > halfRange
                                ? selectedVerticalCell - i
                                : selectedVerticalCell + i;
                    } else {
                        range[i][j] = selectedHorizontalCell;
                    }
                    continue;
                }

                if (HORIZONTAL_DIRECTION.equals(selectedDirection)) {
                    if (j == HORIZONTAL_INDEX) {
                        range[i][j] = selectedHorizontalCell > halfRange
                                ? selectedHorizontalCell - i
                                : selectedHorizontalCell + i;
                    } else {
                        range[i][j] = selectedVerticalCell;
                    }
                }
            }
        }

        for (int row = 0; row < square.length; row++) {
            for (int col = 0; col < square.length; col++) {
                if (row == 0) {
                    square[row][col] = " " + col + SEPARATOR;
                    continue;
                }

                if (col == 0) {
                    square[row][col] = " " + row + SEPARATOR;
                    continue;
                }

                square[row][col] = INITIAL_RENDER_CELL;
            }
        }

        Scanner scanner = new Scanner(System.in);
        int currentVerticalCell;
        int currentHorizontalCell;

        System.out.println("All set. Get ready to rumble!\n");

        game:
        while (true) {
            for (String[] rows : square) {
                System.out.println(String.join("", rows));
            }

            System.out.println("\nPlease select the vertical cell for the shot:");

            while (true) {
                if (!scanner.hasNextInt()) {
                    System.err.println("You entered the wrong number, try again. Please select a range from 1 to 5!");
                    scanner.next();
                    continue;
                }

                break;
            }

            while (true) {
                currentVerticalCell = scanner.nextInt();

                if (currentVerticalCell < 0 || currentVerticalCell > 5) {
                    System.err.println("Your selection is out of shot range. Please select a range from 1 to 5!");
                    continue;
                }

                break;
            }

            System.out.println("Please select the horizontal cell for the shot:");

            while (true) {
                currentHorizontalCell = scanner.nextInt();

                if (currentHorizontalCell < 0 || currentHorizontalCell > 5) {
                    System.err.println("Your selection is out of shot range. Please select a range from 1 to 5!");
                    continue;
                }

                break;
            }


            if (MISSING_RENDER_CELL.equals(square[currentHorizontalCell][currentVerticalCell])
                    || SHOT_RENDER_CELL.equals(square[currentHorizontalCell][currentVerticalCell])) {
                System.err.println("You have already selected this goal, please select another cell!\n");
                continue;
            }

            for (int k = 0; k < range.length; k++) {
                int[] currentCells = range[k];

                if (currentCells[VERTICAL_INDEX] == currentVerticalCell && currentCells[HORIZONTAL_INDEX] == currentHorizontalCell) {
                    square[currentHorizontalCell][currentVerticalCell] = SHOT_RENDER_CELL;

                    if (++shoots == RANG_OF_CELL) {
                        break game;
                    }

                    continue game;
                }

                if (k == range.length - 1) {
                    square[currentHorizontalCell][currentVerticalCell] = MISSING_RENDER_CELL;

                    continue game;
                }
            }
        }

        System.out.println("You have won!");

        for (String[] rows : square) {
            System.out.println(String.join("", rows));
        }
    }
}

