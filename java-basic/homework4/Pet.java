package homework4;

import java.util.Arrays;
import java.util.Objects;

public class Pet {

    static {
        System.out.printf("Завантажується новий клас '%s' \n", Pet.class.getName());
    }

    {
        System.out.printf("Створюється новий об'єкт: '%s'\n", "Pet");
    }

    private String species;
    private String nickname;
    private int age;
    private int trickLevel;
    private String[] habits;

    public Pet() {
    }

    public Pet(String species, String nickname) {
        this.species = species;
        this.nickname = nickname;
    }

    public Pet(String species, String nickname, int age, int trickLevel, String[] habits) {
        this.species = species;
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
    }

    public void eat() {
        System.out.println("Я ї'м!");
    }

    public void respond() {
        System.out.printf("Привіт, хазяїн. Я - %s. Я скучив!\n", nickname);
    }

    public void foul() {
        System.out.println("Потрібно добре замести сліди...");
    }

    public String getSpecies() {
        return species;
    }

    public void setSpecies(String species) {
        this.species = species;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getTrickLevel() {
        return trickLevel;
    }

    public void setTrickLevel(int trickLevel) {
        this.trickLevel = trickLevel;
    }

    public String[] getHabits() {
        return habits;
    }

    public void setHabits(String[] habits) {
        this.habits = habits;
    }

    @Override
    public String toString() {
        return species + "{" + "nickname='" + nickname + "', " + "age=" + age + ", " + "trickLevel=" + trickLevel + ", " + "habits=" + Arrays.toString(habits) + "}";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Pet pet = (Pet) o;

        return Objects.equals(species, pet.getSpecies()) && Objects.equals(nickname, pet.getNickname()) && age == pet.getAge() && trickLevel == pet.getTrickLevel() && Arrays.equals(habits, pet.getHabits());
    }

    @Override
    public int hashCode() {
        final int PRIME = 31;
        int result = species != null ? species.hashCode() : 0;

        result = PRIME * result + (nickname != null ? nickname.hashCode() : 0);
        result = PRIME * result + (habits != null ? Arrays.hashCode(habits) : 0);
        result = PRIME * result + age;
        result = PRIME * result + trickLevel;

        return result;
    }
}
