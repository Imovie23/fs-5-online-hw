package homework4;

public class HappyFamilyMain {

    public static void main(String[] args) {
        simpsonsFamily();
        emptyFamily();
    }

    public static void simpsonsFamily() {
        Pet santaLittleHelpers = new Pet("Dog", "Santa's Little Helper", 10, 50, new String[]{"Sleep"});

        Human homer = new Human("Homer", "Simpson", 1987, 55, new String[][]{{"Monday", "Drink beer"}, {"Friday", "Drink beer"}});
        Human marge = new Human("Marge", "Simpson", 1987);

        Human bart = new Human("Bart", "Simpson", 1997);
        Human lisa = new Human("Lisa", "Simpson", 2000);

        Family simpsons = new Family(marge, homer);

        System.out.println("Init simpsons family\n" + simpsons + '\n');

        simpsons.addChild(bart);

        System.out.println("Add first child\n" + simpsons + '\n');

        homer.greetPet();
        homer.describePet();
        homer.feedPet(true);

        simpsons.setPet(santaLittleHelpers);

        System.out.println("Add pet\n" + simpsons);

        homer.greetPet();
        homer.describePet();
        boolean isFeedPet = homer.feedPet(false);
        System.out.println("Is feed pet: " + isFeedPet);

        simpsons.addChild(lisa);

        System.out.println("Add second child\n" + simpsons + '\n');

        System.out.println("Bart is the son of Homer: " + bart.getFamily().equals(homer.getFamily()) + '\n');
        System.out.println("Bart is the son of Marge: " + bart.getFamily().equals(marge.getFamily()) + '\n');
        System.out.println("Marge is Homer's wife: " + marge.getFamily().equals(homer.getFamily()) + '\n');
        System.out.println("Lisa is the daughter of Homer: " + lisa.getFamily().equals(homer.getFamily()) + '\n');
        System.out.println("Lisa is the daughter of Marge: " + lisa.getFamily().equals(marge.getFamily()) + '\n');
        System.out.println("Lisa is the sister of Bart: " + lisa.getFamily().equals(bart.getFamily()) + '\n');

        System.out.println("Lisa has a family: " + lisa.getFamily());
        System.out.println("The number of the Homer family: " + homer.getFamily().countFamily());

        boolean isDelete = simpsons.deleteChild(lisa);

        System.out.printf("Is delete child: %b\n\n", isDelete);
        System.out.println("Delete child\n" + simpsons + "\n");

        System.out.println("Lisa has a family: " + lisa.getFamily());
        System.out.println("The number of the Homer family: " + homer.getFamily().countFamily());

    }

    public static void emptyFamily() {
        Pet emptyPet = new Pet();
        Human emptyMother = new Human();
        Human emptyFather = new Human();
        Human emptyChild = new Human();
        Family emptyFamily = new Family(emptyMother, emptyFather);

        System.out.println("Empty pet: " + emptyPet + '\n');
        System.out.println("Empty mother: " + emptyMother + '\n');
        System.out.println("Empty father: " + emptyFather + '\n');
        System.out.println("Empty family: " + emptyFamily + '\n');
        System.out.println("Empty child: " + emptyChild + '\n');

        emptyFamily.setPet(emptyPet);

        System.out.println("Add empty pet\n" + emptyFamily);

        emptyFamily.addChild(emptyChild);

        System.out.println("Add empty child\n" + emptyFamily);

        emptyFamily.deleteChild(emptyChild);

        System.out.println("Delete empty child\n" + emptyFamily);

    }
}
