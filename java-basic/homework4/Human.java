package homework4;

import java.util.Arrays;
import java.util.Objects;

public class Human {
    static {
        System.out.printf("Завантажується новий клас '%s' \n", Pet.class.getName());
    }

    {
        System.out.printf("Створюється новий об'єкт: '%s'\n", "Human");
    }
    private String name;
    private String surname;
    private int year;
    private int iq;
    private Family family;
    private String[][] schedule;

    public Human() {
    }

    public Human(String name, String surname, int year) {
        this.name = name;
        this.surname = surname;
        this.year = year;
    }

    public Human(String name, String surname, int year, int iq, String[][] schedule) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.schedule = schedule;
        this.family = family;
    }

    public void greetPet() {
        if (familyHasPet()) {
            System.out.printf("Привіт, %s\n", family.getPet().getNickname());
            return;
        }

        notifyPetMessage();
    }

    public void describePet() {
        if (familyHasPet()) {
            String petTrickLevel = family.getPet().getTrickLevel() > 50 ? "дуже хитрий" : "майже не хитрий";

            System.out.printf("У мене є %s, їй %d років, він %s\n",
                    family.getPet().getSpecies(), family.getPet().getAge(), petTrickLevel);
            return;
        }
        notifyPetMessage();
    }

    public boolean feedPet(boolean isTimeToFeed) {
        if (familyHasPet()) {
            if (isTimeToFeed) {
                return true;
            }

            int randomTrickLevel = 1 + (int) (Math.random() * 100);
            String petNickname = family.getPet().getNickname();
            int petTrickLevel = family.getPet().getTrickLevel();

            if (petTrickLevel > randomTrickLevel) {
                System.out.printf("Хм... годувати %s\n", petNickname);
                return true;
            }

            System.out.printf("Думаю, %s не голодний.\n", petNickname);

            return false;
        }

        notifyPetMessage();
        return false;
    }

    private boolean familyHasPet() {
        return family != null && family.getPet() != null;
    }

    private void notifyPetMessage() {
        System.out.println("There is no pet in the family!!!");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getIq() {
        return iq;
    }

    public void setIq(int iq) {
        this.iq = iq;
    }

    public String[][] getSchedule() {
        return schedule;
    }

    public void setSchedule(String[][] schedule) {
        this.schedule = schedule;
    }

    public Family getFamily() {
        return family;
    }

    public void setFamily(Family family) {
        this.family = family;
    }

    @Override
    public String toString() {
        return "Human{"
                + "name='" + name + "', "
                + "surname='" + surname + "', "
                + "year=" + year + ", "
                + "iq=" + iq + ", "
                + "schedule=" + Arrays.deepToString(schedule)
                + "}";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Human human = (Human) o;

        return Objects.equals(name, human.getName())
                && Objects.equals(surname, human.getSurname())
                && year == human.getYear()
                && iq == human.getIq()
                && Arrays.deepEquals(schedule, human.getSchedule())
                && family.equals(human.getFamily());
    }

    @Override
    public int hashCode() {
        final int PRIME = 31;
        int result = name != null ? name.hashCode() : 0;

        result = PRIME * result + (surname != null ? surname.hashCode() : 0);
        result = PRIME * result + (schedule != null ? Arrays.deepHashCode(schedule) : 0);
        result = PRIME * result + (family != null ? family.hashCode() : 0);
        result = PRIME * result + year;
        result = PRIME * result + iq;

        return result;
    }
}
