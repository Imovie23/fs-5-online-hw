package homework11.exception;

public class FamilyIndexOutOfBoundsException extends Exception {
    public FamilyIndexOutOfBoundsException() {
        super();
    }

    public FamilyIndexOutOfBoundsException(String message) {
        super(message);
    }
}