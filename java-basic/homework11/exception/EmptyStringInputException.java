package homework11.exception;

public class EmptyStringInputException extends Exception {
    public EmptyStringInputException() {
        super();
    }

    public EmptyStringInputException(String message) {
        super(message);
    }
}