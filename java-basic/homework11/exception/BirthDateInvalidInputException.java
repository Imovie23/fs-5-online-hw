package homework11.exception;

public class BirthDateInvalidInputException extends Exception {
    public BirthDateInvalidInputException() {
        super();
    }

    public BirthDateInvalidInputException(String message) {
        super(message);
    }
}