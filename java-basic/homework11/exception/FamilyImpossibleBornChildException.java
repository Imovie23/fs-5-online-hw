package homework11.exception;

public class FamilyImpossibleBornChildException extends Exception {
    public FamilyImpossibleBornChildException() {
        super();
    }

    public FamilyImpossibleBornChildException(String message) {
        super(message);
    }

}