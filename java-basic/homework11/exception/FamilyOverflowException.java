package homework11.exception;

public class FamilyOverflowException extends RuntimeException {
    public FamilyOverflowException() {
        super();
    }

    public FamilyOverflowException(String message) {
        super(message);
    }
}
