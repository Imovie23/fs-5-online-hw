package homework11;

import homework11.view.FamilyConsoleView;
import homework11.controller.FamilyController;
import homework11.dao.CollectionFamilyDao;
import homework11.dao.FamilyDao;
import homework11.service.DefaultFamilyService;
import homework11.service.FamilyService;

import java.util.Scanner;

public class HappyFamilyMain {
    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        FamilyDao familyDao = new CollectionFamilyDao();
        FamilyService familyService = new DefaultFamilyService(familyDao);
        FamilyController familyController = new FamilyController(familyService, scanner);
        FamilyConsoleView familyConsoleView = new FamilyConsoleView(familyController);

        familyConsoleView.show();
    }
}
