package homework11.controller;

import homework11.exception.FamilyImpossibleBornChildException;
import homework11.exception.FamilyIndexOutOfBoundsException;
import homework11.exception.FamilyOverflowException;
import homework11.model.*;
import homework11.service.FamilyService;
import homework11.utils.ConsoleBirthDate;
import homework11.utils.ConsoleUtil;
import homework11.utils.RandomFamilyGenerator;

import static homework11.service.DefaultFamilyService.MAX_NUMBER_IN_FAMILY;

import java.util.List;
import java.util.Scanner;

public class FamilyController {
    private final FamilyService familyService;
    private final Scanner scanner;

    public FamilyController(FamilyService familyService, Scanner scanner) {
        this.familyService = familyService;
        this.scanner = scanner;
    }

    public List<Family> getAllFamilies() {
        return this.familyService.getAllFamilies();
    }

    public Family adoptChild(Family family, Human child) {
        try {
            return this.familyService.adoptChild(family, child);
        } catch (FamilyOverflowException e) {
            System.err.println(e.getMessage());
            return null;
        }
    }

    public Family adoptChild() {
        int numberPeopleInFamily = ConsoleUtil.getInputNumberValue(scanner, "Please enter a family ID");

        try {
            Family family = getFamilyById(numberPeopleInFamily);

            if (family.countFamily() + 1 > MAX_NUMBER_IN_FAMILY) {
                throw new FamilyOverflowException("It is not possible to add a child to this family. Limit exceeded");
            }

            int startYearOfChildBirth = isPossibleBornChild(family);

            Human child = consoleRequestHumanData(startYearOfChildBirth);

            return adoptChild(family, child);
        } catch (FamilyIndexOutOfBoundsException | FamilyImpossibleBornChildException | FamilyOverflowException e) {
            System.err.println(e.getMessage());
            return null;
        }
    }

    public Family bornChild(Family family, String boyName, String girlName) {
        try {
            return this.familyService.bornChild(family, boyName, girlName);
        } catch (FamilyOverflowException e) {
            System.err.println(e.getMessage());
            return null;
        }
    }

    public Family bornChild() {
        try {
            int numberPeopleInFamily = ConsoleUtil.getInputNumberValue(scanner, "Please enter a family ID");
            Family family = getFamilyById(numberPeopleInFamily);

            if (family.countFamily() + 1 > MAX_NUMBER_IN_FAMILY) {
                throw new FamilyOverflowException("It is not possible to add a child to this family. Limit exceeded");
            }

            isPossibleBornChild(family);

            String boyName = ConsoleUtil.getInputStringValue(scanner,
                    "Please enter boy's name", "Invalid boy name, please try again");
            String girlName = ConsoleUtil.getInputStringValue(scanner,
                    "Please enter girl's name", "Invalid girl name, please try again");


            return bornChild(family, boyName, girlName);
        } catch (FamilyIndexOutOfBoundsException | FamilyImpossibleBornChildException | FamilyOverflowException e) {
            System.err.println(e.getMessage());
            return null;
        }
    }

    private int isPossibleBornChild(Family family) throws FamilyImpossibleBornChildException {
        int motherYear = ConsoleBirthDate.getYearFromDate(family.getMother().getBirthDate());
        int fatherYear = ConsoleBirthDate.getYearFromDate(family.getFather().getBirthDate());

        int minChildYear = Math.max(motherYear, fatherYear);
        int startYearOfChildBirth = Math.min(minChildYear, ConsoleBirthDate.CURRENT_YEAR) + ConsoleBirthDate.ADULT_YEAR;

        int diffYears = ConsoleBirthDate.CURRENT_YEAR - startYearOfChildBirth;

        if (diffYears < 0) {
            String errorMessage = "The family has members younger than %d years. It is impossible to give birth to a child\n ".formatted(ConsoleBirthDate.ADULT_YEAR);
            throw new FamilyImpossibleBornChildException(errorMessage);
        }

        return startYearOfChildBirth;
    }

    public Family getFamilyById(int index) throws FamilyIndexOutOfBoundsException {
        List<Family> families = getAllFamilies();
        int length = families.size() - 1;

        if (index < 0 || index > length) {
            throw new FamilyIndexOutOfBoundsException("Invalid family index");
        }

        return this.familyService.getFamilyById(index);
    }

    public List<AbstractPet> getPets(int familyIndex) {
        return this.familyService.getPets(familyIndex);
    }

    public void getFamiliesBiggerThan(int numberPeopleInFamily) {
        this.familyService.getFamiliesBiggerThan(numberPeopleInFamily);
    }

    public void displayFamiliesBiggerThan() {
        int numberPeopleInFamily = ConsoleUtil.getInputNumberValue(scanner, "Please enter the quantity");

        if (numberPeopleInFamily < 0) {
            System.err.println("Invalid quantity, please try again!!!");
            return;
        }

        getFamiliesBiggerThan(numberPeopleInFamily);
    }

    public void getFamiliesLessThan(int numberPeopleInFamily) {
        this.familyService.getFamiliesLessThan(numberPeopleInFamily);
    }

    public void displayFamiliesLessThan() {
        int numberPeopleInFamily = ConsoleUtil.getInputNumberValue(scanner, "Please enter the quantity");

        if (numberPeopleInFamily < 0) {
            System.err.println("Invalid quantity, please try again!!!");
            return;
        }

        getFamiliesLessThan(numberPeopleInFamily);
    }

    public int count() {
        return this.familyService.count();
    }

    public int countFamiliesWithMemberNumber(int numberPeopleInFamily) {
        return this.familyService.countFamiliesWithMemberNumber(numberPeopleInFamily);
    }

    public void displayCountFamiliesWithMemberNumber() {
        int numberPeopleInFamily = ConsoleUtil.getInputNumberValue(scanner, "Please enter the quantity");

        if (numberPeopleInFamily < 0) {
            System.err.println("Invalid quantity, please try again!!!");
            return;
        }

        int countFamilies = countFamiliesWithMemberNumber(numberPeopleInFamily);

        System.out.printf("Number of families with member number (%d): %d\n", numberPeopleInFamily, countFamilies);
    }

    public void displayAllFamilies() {
        this.familyService.displayAllFamilies();
    }

    public void createNewFamily(Human woman, Human man) {
        this.familyService.createNewFamily(woman, man);
    }

    public void createNewFamily() {
        Human mother = consoleRequestHumanData("mother's");
        Human father = consoleRequestHumanData("father's");

        createNewFamily(mother, father);
    }

    private Human consoleRequestHumanData(String gender) {
        String name = ConsoleUtil.getInputStringValue(scanner,
                "Please enter %s name".formatted(gender),
                "Invalid name, please try again");

        String surname = ConsoleUtil.getInputStringValue(scanner,
                "Please enter %s surname".formatted(gender),
                "Invalid surname, please try again");

        ConsoleBirthDate requestConsoleBirthDate = new ConsoleBirthDate(scanner);

        String birthDate = requestConsoleBirthDate.consoleRequestAndValidateBirthDate();

        int iq = consoleRequestAndValidateIq(gender);

        return new Human(name, surname, birthDate, iq);
    }

    private Human consoleRequestHumanData(int year) {
        String name = ConsoleUtil.getInputStringValue(scanner,
                "Please enter child's name",
                "Invalid name, please try again");

        String surname = ConsoleUtil.getInputStringValue(scanner,
                "Please enter child's surname",
                "Invalid surname, please try again");

        ConsoleBirthDate requestConsoleBirthDate = new ConsoleBirthDate(scanner, year);

        String birthDate = requestConsoleBirthDate.consoleRequestAndValidateBirthDate();

        int iq = consoleRequestAndValidateIq("child's");

        return new Human(name, surname, birthDate, iq);
    }

    private int consoleRequestAndValidateIq(String gender) {
        int iq;

        while (true) {
            iq = ConsoleUtil.getInputNumberValue(scanner,
                    "Please enter %s iq".formatted(gender));

            if (iq <= 0 || iq > 200) {
                System.err.println("Invalid iq, please try again");
                continue;
            }

            break;
        }
        return iq;
    }

    public void deleteFamilyByIndex(int index) {
        List<Family> families = getAllFamilies();
        int length = families.size() - 1;

        if (index < 0 || index > length) {
            System.err.println("Invalid family index");
            return;
        }

        this.familyService.deleteFamilyByIndex(index);
    }

    public void deleteFamilyByIndex() {
        int index = ConsoleUtil.getInputNumberValue(scanner, "Please enter a family ID");

        deleteFamilyByIndex(index);
    }

    public void deleteAllChildrenOlderThen(int year) {
        this.familyService.deleteAllChildrenOlderThen(year);
    }

    public void deleteAllChildrenOlderThen() {
        int year = ConsoleUtil.getInputNumberValue(scanner, "Please enter year");

        if (year < 0) {
            System.err.println("Invalid year");
            return;
        }

        deleteAllChildrenOlderThen(year);
    }

    public void addPets(int index, AbstractPet pet) {
        this.familyService.addPets(index, pet);
    }

    public void generateRandomFamilies() {
        RandomFamilyGenerator randomFamilyGenerator = new RandomFamilyGenerator(familyService);

        randomFamilyGenerator.generateRandomFamilies();
    }
}
