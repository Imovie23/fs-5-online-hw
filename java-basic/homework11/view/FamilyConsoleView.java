package homework11.view;

import homework11.controller.FamilyController;
import homework11.utils.ConsoleUtil;

import java.util.Scanner;
import java.util.stream.Stream;

public class FamilyConsoleView {
    private final FamilyController familyController;
    private final Scanner scanner = new Scanner(System.in);
    private final String ERROR_MESSAGE = "Such a menu item does not exist, please try again!!!\n";

    public FamilyConsoleView(FamilyController familyController) {
        this.familyController = familyController;
    }

    public void show() {
        FamilyMenuItem[] menuItems = FamilyMenuItem.values();

        String menu = buildMenu(menuItems);

        FamilyMenuItem menuItem;
        int choose;

        while (true) {
            System.out.println(menu);

            choose = ConsoleUtil.getInputNumberValue(scanner,
                    "Please select a menu item: ",
                    ERROR_MESSAGE) - 1;
            menuItem = getMenuItem(menuItems, choose);

            switch (menuItem) {
                case FILL_TEST_DATA -> familyController.generateRandomFamilies();
                case SHOW_FAMILIES -> familyController.displayAllFamilies();
                case SHOW_FAMILIES_BIGGER_THEN -> familyController.displayFamiliesBiggerThan();
                case SHOW_FAMILIES_LESS_THEN -> familyController.displayFamiliesLessThan();
                case SHOW_FAMILIES_WITH_MEMBER_NUMBER -> familyController.displayCountFamiliesWithMemberNumber();
                case CREATE_FAMILY -> familyController.createNewFamily();
                case DELETE_FAMILY -> familyController.deleteFamilyByIndex();
                case EDIT_FAMILY -> showChildrenMenu();
                case DELETE_ALL_CHILDREN_OLDER_THAN -> familyController.deleteAllChildrenOlderThen();
                case EXIT -> System.exit(0);
                default -> System.err.println(ERROR_MESSAGE);
            }
        }
    }

    private void showChildrenMenu() {
        ChildrenMenuItem[] menuItems = ChildrenMenuItem.values();
        String menu = buildMenu(menuItems);

        ChildrenMenuItem menuItem;
        int choose;

        closeChildMenu:
        while (true) {
            System.out.println(menu);

            choose = ConsoleUtil.getInputNumberValue(scanner,
                    "Please select a menu item: ",
                    ERROR_MESSAGE) - 1;

            menuItem = getMenuItem(menuItems, choose);

            switch (menuItem) {
                case BORN_CHILD -> familyController.bornChild();
                case ADOPT_CHILD -> familyController.adoptChild();
                case BACK_MAIN_MENU -> {
                    break closeChildMenu;
                }
                default -> System.err.println(ERROR_MESSAGE);
            }
        }
    }

    private <T extends MenuItem<T>> String buildMenu(T[] menuItems) {
        return Stream.of(menuItems).filter(menuItem -> !menuItem.getUnknownItem().equals(menuItem))
                .map(menuItem -> "%d. %s".formatted(menuItem.ordinal() + 1, menuItem.getDescription()))
                .reduce("", ((acc, stringFamilyMenuItem) -> acc + stringFamilyMenuItem + "\n"));
    }

    private <T extends MenuItem<T>> T getMenuItem(T[] menuItems, int choose) {
        try {
            return menuItems[choose];
        } catch (IndexOutOfBoundsException e) {
            T menuItem = menuItems[0];

            return menuItem.getUnknownItem();
        }
    }

    private interface MenuItem<T> {
        String getDescription();

        T getUnknownItem();

        int ordinal();
    }

    enum FamilyMenuItem implements MenuItem<FamilyMenuItem> {
        FILL_TEST_DATA("Заповнити тестовими даними."),
        SHOW_FAMILIES("Відобразити весь список сімей."),
        SHOW_FAMILIES_BIGGER_THEN("Відобразити список сімей, де кількість людей більша за задану."),
        SHOW_FAMILIES_LESS_THEN("Відобразити список сімей, де кількість людей менша за задану."),
        SHOW_FAMILIES_WITH_MEMBER_NUMBER("Підрахувати кількість сімей, де кількість членів дорівнює."),
        CREATE_FAMILY("Створити нову родину."),
        DELETE_FAMILY("Видалити сім'ю за індексом сім'ї у загальному списку."),
        EDIT_FAMILY("Редагувати сім'ю за індексом сім'ї у загальному списку."),
        DELETE_ALL_CHILDREN_OLDER_THAN("Видалити всіх дітей старше віку."),
        EXIT("Вихід."),

        UNKNOWN("");

        private final String description;

        FamilyMenuItem(String description) {
            this.description = description;
        }

        @Override
        public String getDescription() {
            return this.description;
        }

        @Override
        public FamilyMenuItem getUnknownItem() {
            return FamilyMenuItem.UNKNOWN;
        }
    }

    enum ChildrenMenuItem implements MenuItem<ChildrenMenuItem> {
        BORN_CHILD("Народити дитину."),
        ADOPT_CHILD("Усиновити дитину."),
        BACK_MAIN_MENU("Повернутися до головного меню."),
        UNKNOWN("");

        private final String description;

        ChildrenMenuItem(String description) {
            this.description = description;
        }

        @Override
        public String getDescription() {
            return this.description;
        }

        @Override
        public ChildrenMenuItem getUnknownItem() {
            return ChildrenMenuItem.UNKNOWN;
        }
    }

}
