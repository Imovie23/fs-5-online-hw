package homework11.utils;

import homework11.exception.FamilyOverflowException;
import homework11.model.*;
import homework11.service.FamilyService;

import java.time.LocalDate;
import java.time.YearMonth;

public class RandomFamilyGenerator {
    private final FamilyService familyService;

    public RandomFamilyGenerator(FamilyService familyService) {
        this.familyService = familyService;
    }

    public void generateRandomFamilies() {
        int randomFamilyNumber = (int) Math.round(generateRandomInRange(1, 5));

        for (int i = 0; i < randomFamilyNumber; i++) {
            randomFamily();
        }

    }

    private void randomFamily() {
        int randomChildNumber = (int) Math.round(generateRandomInRange(1, 5));

        String womanName = randomGirlName();
        String womanBirthDate = randomBirthDate();
        int womanIq = randomIq();

        String manName = randomBoyName();
        String surname = randomSurname();
        String manBirthDate = randomBirthDate();
        int manIq = randomIq();

        int minChildBirthYear = Math.min(
                Math.max(getYearFromDate(womanBirthDate), getYearFromDate(manBirthDate)) + ConsoleBirthDate.ADULT_YEAR,
                LocalDate.now().getYear()
        );

        Human woman = new Human(womanName, surname, womanBirthDate, womanIq);
        Human man = new Human(manName, surname, manBirthDate, manIq);

        Family family = new Family(woman, man);

        for (int i = 0; i < randomChildNumber; i++) {
            try {
                familyService.adoptChild(family, randomChild(minChildBirthYear));
            } catch (FamilyOverflowException e) {
                break;
            }
        }

    }

    private int getYearFromDate(String date) {
        LocalDate currentDate = LocalDate.parse(date, ConsoleBirthDate.FORMATTER);
        return currentDate.getYear();
    }

    private Human randomChild(int minBirthYear) {
        int randomGenderNumber = (int) Math.round(generateRandomInRange(0, 1));

        Human child;
        String name;
        String surname = randomSurname();
        String birthDate = randomBirthDate(minBirthYear);
        int iq = randomIq();

        if (randomGenderNumber == 0) {
            name = randomGirlName();

            child = new Woman(name, surname, birthDate, iq);
        } else {
            name = randomBoyName();

            child = new Man(name, surname, birthDate, iq);
        }


        return child;
    }

    private String randomBoyName() {
        BoyName[] boys = BoyName.values();
        int length = boys.length - 1;

        int randomBoysNumber = (int) Math.round(generateRandomInRange(0, length));

        return boys[randomBoysNumber].getBoyName();
    }

    private String randomGirlName() {
        GirlName[] girls = GirlName.values();
        int length = girls.length - 1;

        int randomGirlNumber = (int) Math.round(generateRandomInRange(0, length));

        return girls[randomGirlNumber].getGirlName();
    }

    private String randomSurname() {
        final String lexicon = "ABCDEFGHIJKLMNOPQRSTUVWXYZ12345674890";
        StringBuilder builder = new StringBuilder();
        int length = (int) generateRandomInRange(4, 10);

        for (int i = 0; i < length; i++) {
            builder.append(lexicon.charAt((int) generateRandomInRange(0, lexicon.length())));
        }

        return builder.toString();
    }

    private String randomBirthDate() {
        int year = (int) generateRandomInRange(ConsoleBirthDate.MIN_YEAR_OF_BIRTH, ConsoleBirthDate.MAX_ADDED_YEAR_OF_BIRTH);
        int month = (int) generateRandomInRange(1, 12);

        YearMonth yearMonthObject = YearMonth.of(year, month);

        return buildBirthDate(yearMonthObject, month, year);
    }

    private String randomBirthDate(int from) {
        int difference = ConsoleBirthDate.MAX_ADDED_YEAR_OF_BIRTH - (from - ConsoleBirthDate.MIN_YEAR_OF_BIRTH);
        int to = Math.max(difference, 0);

        int year = (int) generateRandomInRange(from, to);
        int month = (int) generateRandomInRange(1, 12);

        YearMonth yearMonthObject = YearMonth.of(year, month);

        return buildBirthDate(yearMonthObject, month, year);
    }

    private String buildBirthDate(YearMonth yearMonthObject, int month, int year) {
        int daysInMonth = yearMonthObject.lengthOfMonth();

        int day = (int) generateRandomInRange(1, daysInMonth);

        String stringMonth = String.valueOf(month);
        stringMonth = stringMonth.length() == 1 ? "0" + stringMonth : stringMonth;

        String stringDay = String.valueOf(day);
        stringDay = stringDay.length() == 1 ? "0" + stringDay : stringDay;

        return "%s/%s/%d".formatted(stringDay, stringMonth, year);
    }

    private int randomIq() {
        return (int) generateRandomInRange(0, 200);
    }

    private double generateRandomInRange(int start, int end) {
        return start + Math.random() * end;
    }
}
