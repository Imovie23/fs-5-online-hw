package homework11.utils;

import homework11.exception.EmptyStringInputException;

import java.util.NoSuchElementException;
import java.util.Scanner;

public class ConsoleUtil {

    private static final String NUMBER_ERROR = "Wrong number entered, please try again";

    public static int getInputNumberValue(Scanner scanner, String message, String error) {
        while (true) {
            System.out.println(message);
            try {
                return Integer.parseInt(scanner.nextLine());
            } catch (NumberFormatException e) {
                System.err.println(error);
            }
        }
    }

    public static int getInputNumberValue(Scanner scanner, String message) {
        while (true) {
            System.out.println(message);
            try {
                return Integer.parseInt(scanner.nextLine());
            } catch (NumberFormatException e) {
                System.err.println(NUMBER_ERROR);
            }
        }
    }

    public static String getInputStringValue(Scanner scanner, String message, String error) {
        while (true) {
            System.out.println(message);
            try {
                String value = scanner.nextLine();

                if (value.equals("")) {
                    throw new EmptyStringInputException(error);
                }

                return value;
            } catch (NoSuchElementException | EmptyStringInputException e) {
                System.err.println(error);
            }
        }
    }
}
