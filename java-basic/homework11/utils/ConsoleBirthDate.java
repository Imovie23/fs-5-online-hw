package homework11.utils;

import homework11.exception.BirthDateInvalidInputException;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

public class ConsoleBirthDate {
    public static final String BIRTH_DATE_FORMAT = "dd/MM/yyyy";
    public static final int MIN_YEAR_OF_BIRTH = 1900;
    public static final int CURRENT_YEAR = LocalDate.now().getYear();
    public static final int MAX_ADDED_YEAR_OF_BIRTH = CURRENT_YEAR - MIN_YEAR_OF_BIRTH;
    public static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern(BIRTH_DATE_FORMAT);
    public static final int MAX_YEAR_LIFE = 122;
    public static final int ADULT_YEAR = 18;
    private int maxAgeOfHuman;
    private final Scanner scanner;

    public ConsoleBirthDate(Scanner scanner) {
        this.scanner = scanner;
    }

    public ConsoleBirthDate(Scanner scanner, int year) {
        this.scanner = scanner;
        this.maxAgeOfHuman = CURRENT_YEAR - year;
    }

    public String consoleRequestAndValidateBirthDate() {
        int currentYear = consoleRequestAndValidateYear();
        int currentMonth = consoleRequestAndValidateMonth();
        int currentDay = consoleRequestAndValidateDay(currentYear, currentMonth);

        String month = String.valueOf(currentMonth);
        month = month.length() == 1 ? "0" + month : month;

        String day = String.valueOf(currentDay);
        day = day.length() == 1 ? "0" + day : day;

        return "%s/%s/%d".formatted(day, month, currentYear);
    }

    public int consoleRequestAndValidateYear() {
        while (true) {
            int year = ConsoleUtil.getInputNumberValue(scanner, "Please enter year of birth");

            System.out.println(year);

            try {
                validateYear(year);

                return year;
            } catch (BirthDateInvalidInputException e) {
                System.err.println(e.getMessage());
            }
        }

    }

    private void validateYear(int year) throws BirthDateInvalidInputException {
        LocalDate now = LocalDate.now();
        LocalDate dateFromYear = LocalDate.of(year, 1, 1);
        Period intervalPeriod = Period.between(dateFromYear, now);
        int ageFromYear = intervalPeriod.getYears();

        if (maxAgeOfHuman > 0 && ageFromYear > maxAgeOfHuman) {
            int startYearOfBirth = Math.min(CURRENT_YEAR - maxAgeOfHuman, CURRENT_YEAR);

            String errorMessage = "Year of birth can start from %d to %d\n".formatted(startYearOfBirth, CURRENT_YEAR);

            throw new BirthDateInvalidInputException(errorMessage);
        }

        if (year <= 0 || year > CURRENT_YEAR || ageFromYear > MAX_YEAR_LIFE) {
            throw new BirthDateInvalidInputException("Incorrect number year, please try again");
        }
    }

    public int consoleRequestAndValidateMonth() {
        while (true) {
            int month = ConsoleUtil.getInputNumberValue(scanner, "Please enter month of birth");

            try {
                validateMonth(month);

                return month;
            } catch (BirthDateInvalidInputException e) {
                System.err.println(e.getMessage());
            }
        }
    }

    private void validateMonth(int month) throws BirthDateInvalidInputException {
        if (month <= 0 || month > 12) {
            throw new BirthDateInvalidInputException("Incorrect number month, please try again");
        }
    }

    public int consoleRequestAndValidateDay(int year, int month) {
        while (true) {
            int day = ConsoleUtil.getInputNumberValue(scanner, "Please enter day of birth");

            try {
                validateDay(day, year, month);

                return day;
            } catch (BirthDateInvalidInputException e) {
                System.err.println(e.getMessage());
            }
        }
    }

    private void validateDay(int day, int year, int month) throws BirthDateInvalidInputException {
        YearMonth yearMonthObject = YearMonth.of(year, month);
        int daysInMonth = yearMonthObject.lengthOfMonth();

        String errorMessage = "Invalid day number, the selected month has a range of days from 1 to %d\n".formatted(daysInMonth);

        if (day <= 0) {
            throw new BirthDateInvalidInputException(errorMessage);
        }

        if (day > daysInMonth) {
            throw new BirthDateInvalidInputException(errorMessage);
        }
    }

    public static int getYearFromDate(long birthDate) {
        return Instant.ofEpochMilli(birthDate)
                .atZone(ZoneId.systemDefault())
                .toLocalDate().getYear();
    }

}
