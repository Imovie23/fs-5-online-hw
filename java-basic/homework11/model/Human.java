package homework11.model;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Map;
import java.util.Objects;

public class Human implements PrettyFormat {
    private final String BIRTH_DATE_FORMAT = "dd/MM/yyyy";
    private final DateTimeFormatter formatter = DateTimeFormatter.ofPattern(BIRTH_DATE_FORMAT);
    private String name;
    private String surname;
    private long birthDate;
    private int iq;
    private Family family;
    private Map<String, String> schedule;

    public Human() {
    }

    public Human(String name) {
        this.name = name;
    }

    public Human(String name, String surname, String birthDate) {
        this.name = name;
        this.surname = surname;
        this.birthDate = toBirthDateToTimestamp(birthDate);
    }

    public Human(String name, String surname, String birthDate, int iq) {
        this.name = name;
        this.surname = surname;
        this.birthDate = toBirthDateToTimestamp(birthDate);
        this.iq = iq;
    }

    public Human(String name, String surname, String birthDate, int iq, Map<String, String> schedule) {
        this.name = name;
        this.surname = surname;
        this.birthDate = toBirthDateToTimestamp(birthDate);
        this.iq = iq;
        this.schedule = schedule;
    }

    public void greetPets() {
        if (familyHasPets()) {
            for (AbstractPet pet : family.getPets()) {
                System.out.printf("Привіт, %s\n", pet.getNickname());
            }
            return;
        }

        notifyPetMessage();
    }

    public void describePets() {
        if (familyHasPets()) {
            for (AbstractPet pet : family.getPets()) {
                String petTrickLevel = pet.getTrickLevel() > 50 ? "дуже хитрий" : "майже не хитрий";

                System.out.printf("У мене є %s, їй %d років, він %s\n",
                        pet.getSpecies(), pet.getAge(), petTrickLevel);
            }

            return;
        }
        notifyPetMessage();
    }

    public boolean feedAllPets(boolean isTimeToFeed) {
        if (familyHasPets()) {
            if (isTimeToFeed) {
                return true;
            }

            int randomTrickLevel;
            int petTrickLevel;
            int countOfFeedPets = 0;
            String petNickname;


            for (AbstractPet pet : family.getPets()) {
                randomTrickLevel = 1 + (int) (Math.random() * 100);
                petNickname = pet.getNickname();
                petTrickLevel = pet.getTrickLevel();


                if (petTrickLevel > randomTrickLevel) {
                    countOfFeedPets++;
                    System.out.printf("Хм... годувати %s\n", petNickname);
                    continue;
                }

                System.out.printf("Думаю, %s не голодний.\n", petNickname);

            }


            return countOfFeedPets == family.getPets().size();
        }

        notifyPetMessage();
        return false;
    }

    protected boolean familyHasPets() {
        return family != null && family.getPets() != null;
    }

    protected void notifyPetMessage() {
        System.out.println("There is no pet in the family!!!");
    }

    public String describeAge() {
        return Instant.ofEpochMilli(this.birthDate)
                .atZone(ZoneId.systemDefault())
                .toLocalDate().format(formatter);
    }

    private long toBirthDateToTimestamp(String formattedDate) {
        return LocalDate.parse(formattedDate, formatter).atStartOfDay().atZone(ZoneId.systemDefault())
                .toInstant().toEpochMilli();
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public long getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(long birthDate) {
        this.birthDate = birthDate;
    }

    public int getIq() {
        return iq;
    }

    public void setIq(int iq) {
        this.iq = iq;
    }

    public Map<String, String> getSchedule() {
        return schedule;
    }

    public void setSchedule(Map<String, String> schedule) {
        this.schedule = schedule;
    }

    public Family getFamily() {
        return family;
    }

    public void setFamily(Family family) {
        this.family = family;
    }

    @Override
    public String prettyFormat() {
        return "{"
                + "name='" + name + "', "
                + "surname='" + surname + "', "
                + "birthDate=" + describeAge() + ", "
                + "iq=" + iq
                + (schedule == null ? "" : ", schedule=" + schedule)
                + "}";
    }

    @Override
    public String toString() {
        return "Human{"
                + "name='" + name + "', "
                + "surname='" + surname + "', "
                + "birthDate=" + describeAge() + ", "
                + "iq=" + iq + ", "
                + "schedule=" + schedule
                + "}";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Human human = (Human) o;

        return Objects.equals(name, human.getName())
                && Objects.equals(surname, human.getSurname())
                && birthDate == human.birthDate
                && iq == human.getIq()
                && schedule.equals(human.getSchedule())
                && family.equals(human.getFamily());
    }

    @Override
    public int hashCode() {
        final int PRIME = 31;
        int result = name != null ? name.hashCode() : 0;

        result = PRIME * result + (surname != null ? surname.hashCode() : 0);
        result = PRIME * result + (schedule != null ? schedule.hashCode() : 0);
        result = PRIME * result + (family != null ? family.hashCode() : 0);
        result = PRIME * result + (int) (birthDate ^ (birthDate >>> 32));
        result = PRIME * result + iq;

        return result;
    }
}
