package homework11.model;

public enum Species {
    ROBO_CAT, DOG, FISH, DOMESTIC_CAT, UNKNOWN
}
