package homework11.service;

import homework11.dao.FamilyDao;
import homework11.exception.FamilyOverflowException;
import homework11.model.AbstractPet;
import homework11.model.Family;
import homework11.model.Human;
import homework11.model.Man;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

public class DefaultFamilyService implements FamilyService {

    private final FamilyDao familyDao;

    public static final int MAX_NUMBER_IN_FAMILY = 6;

    public DefaultFamilyService(FamilyDao familyDao) {
        this.familyDao = familyDao;
    }

    @Override
    public Family adoptChild(Family family, Human child) throws FamilyOverflowException {
        if (isEmptyFamily(family) || isEmptyChild(child)) {
            return null;
        }

        if (family.countFamily() + 1 > MAX_NUMBER_IN_FAMILY) {
            throw new FamilyOverflowException("It is not possible to add a child to this family. Limit exceeded");
        }

        List<Family> families = getAllFamilies();

        int familyPosition = families.indexOf(family);

        family.addChild(child);

        if (familyPosition == -1) {
            this.familyDao.saveFamily(family);
            return family;
        }

        return family;
    }

    private boolean isEmptyChild(Human child) {
        return child == null;
    }

    @Override
    public Family bornChild(Family family, String boyName, String girlName) throws FamilyOverflowException {
        if (isEmptyFamily(family)) {
            return null;
        }

        if (family.countFamily() + 1 > MAX_NUMBER_IN_FAMILY) {
            throw new FamilyOverflowException("It is not possible to add a child to this family. Limit exceeded");
        }

        Human child = family.bornChild();

        if (child.getClass().equals(Man.class)) {
            child.setName(boyName);
        } else {
            child.setName(girlName);
        }

        adoptChild(family, child);

        return family;
    }

    private boolean isEmptyFamily(Family family) {
        return family == null;
    }

    @Override
    public void getFamiliesBiggerThan(int numberPeopleInFamily) {
        if (isNumberLessThanZero(numberPeopleInFamily)) {
            return;
        }

        List<Family> families = getAllFamilies();

        families.stream()
                .filter(family -> family.countFamily() > numberPeopleInFamily)
                .forEach(family -> System.out.println(family.prettyFormat()));
    }

    @Override
    public void getFamiliesLessThan(int numberPeopleInFamily) {
        if (isNumberLessThanZero(numberPeopleInFamily)) {
            return;
        }

        List<Family> families = getAllFamilies();

        families.stream()
                .filter(family -> family.countFamily() < numberPeopleInFamily)
                .forEach(family -> System.out.println(family.prettyFormat()));

    }

    @Override
    public int countFamiliesWithMemberNumber(int numberPeopleInFamily) {
        if (isNumberLessThanZero(numberPeopleInFamily)) {
            return 0;
        }

        List<Family> families = getAllFamilies();

        return (int) families.stream()
                .filter(family -> family.countFamily() == numberPeopleInFamily)
                .count();
    }

    @Override
    public int count() {
        return getAllFamilies().size();
    }

    @Override
    public void displayAllFamilies() {
        List<Family> families = this.getAllFamilies();

        if (families.size() == 0) {
            System.out.println(families);
            return;
        }

        IntStream.range(0, families.size())
                .forEach(index ->
                        System.out.println("№" + (index) + ": " + families.get(index).prettyFormat()));

    }

    @Override
    public void deleteAllChildrenOlderThen(int year) {
        if (year < 0) {
            return;
        }

        List<Family> families = getAllFamilies();

        families.forEach(family -> family.getChildren()
                .removeIf(child -> year < getYearFromTimestamp(child.getBirthDate())));

    }

    private int getYearFromTimestamp(long timestamp) {
        return Instant.ofEpochMilli(timestamp)
                .atZone(ZoneId.systemDefault())
                .toLocalDate().until(LocalDate.now()).getYears();
    }

    @Override
    public List<Family> getAllFamilies() {
        return this.familyDao.getAllFamilies();
    }

    @Override
    public List<AbstractPet> getPets(int familyIndex) {
        List<AbstractPet> emptyList = new ArrayList<>();

        if (isNumberLessThanZero(familyIndex)) {
            return emptyList;
        }

        Family family = getFamilyById(familyIndex);

        if (family == null) {
            return emptyList;
        }

        return family.getPets();
    }

    @Override
    public void addPets(int familyIndex, AbstractPet pet) {
        if (isNumberLessThanZero(familyIndex) || pet == null) {
            return;
        }

        Family family = getFamilyById(familyIndex);

        if (family == null) {
            return;
        }

        this.familyDao.deleteFamily(familyIndex);

        family.addPet(pet);

        this.familyDao.saveFamily(family);
    }

    private boolean isNumberLessThanZero(int num) {
        return num < 0;
    }

    @Override
    public Family getFamilyById(int index) {
        return this.familyDao.getFamilyByIndex(index);
    }

    @Override
    public void deleteFamilyByIndex(int index) {
        this.familyDao.deleteFamily(index);
    }

    @Override
    public void createNewFamily(Human woman, Human man) {
        Family family = new Family(woman, man);

        this.familyDao.saveFamily(family);

    }

}
