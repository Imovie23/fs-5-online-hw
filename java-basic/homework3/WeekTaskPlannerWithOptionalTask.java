package homework3;

import java.util.Scanner;

public class WeekTaskPlannerWithOptionalTask {
    public static final String SUNDAY = "sunday";
    public static final String MONDAY = "monday";
    public static final String TUESDAY = "tuesday";
    public static final String WEDNESDAY = "wednesday";
    public static final String THURSDAY = "thursday";
    public static final String FRIDAY = "friday";
    public static final String SATURDAY = "saturday";
    public static final String EXIT = "exit";
    public static final String CHANGE = "change";
    public static final String RESCHEDULE = "reschedule";
    public static final int DAYS_OF_WEEK = 7;
    public static String[][] schedules;
    public static int WEEKDAY_INDEX = 0;
    public static int TASKS_INDEX = 1;
    public static Scanner scanner;

    public static void main(String[] args) {
        scanner = new Scanner(System.in);

        buildWeeklyTasks();
        tasksRunner();
    }

    public static void buildWeeklyTasks() {
        String[][] weekSchedules = new String[DAYS_OF_WEEK][2];

        weekSchedules[0][WEEKDAY_INDEX] = SUNDAY;
        weekSchedules[0][TASKS_INDEX] = "Sleep;";

        weekSchedules[1][WEEKDAY_INDEX] = MONDAY;
        weekSchedules[1][TASKS_INDEX] = "Do home work;";

        weekSchedules[2][WEEKDAY_INDEX] = TUESDAY;
        weekSchedules[2][TASKS_INDEX] = "Go to courses;";

        weekSchedules[3][WEEKDAY_INDEX] = WEDNESDAY;
        weekSchedules[3][TASKS_INDEX] = "Watch a film;";

        weekSchedules[4][WEEKDAY_INDEX] = THURSDAY;
        weekSchedules[4][TASKS_INDEX] = "Learn java;";

        weekSchedules[5][WEEKDAY_INDEX] = FRIDAY;
        weekSchedules[5][TASKS_INDEX] = "Go to nature;";

        weekSchedules[6][WEEKDAY_INDEX] = SATURDAY;
        weekSchedules[6][TASKS_INDEX] = "Walk with children;";

        schedules = weekSchedules;
    }

    public static void tasksRunner() {
        String operation;

        runner:
        while (true) {
            System.out.println("Please, input the day of the week:");

            operation = scanner.nextLine();
            operation = toValidFormat(operation);

            switch (operation) {
                case SUNDAY:
                    showTaskMessage(SUNDAY);
                    break;
                case MONDAY:
                    showTaskMessage(MONDAY);
                    break;
                case TUESDAY:
                    showTaskMessage(TUESDAY);
                    break;
                case WEDNESDAY:
                    showTaskMessage(WEDNESDAY);
                    break;
                case THURSDAY:
                    showTaskMessage(THURSDAY);
                    break;
                case FRIDAY:
                    showTaskMessage(FRIDAY);
                    break;
                case SATURDAY:
                    showTaskMessage(SATURDAY);
                    break;
                case EXIT:
                    System.out.println("Successful exit from the weekly task planner");
                    break runner;
                default:
                    rescheduleTaskOrShowError(operation);
                    break;
            }
        }
    }

    public static String toValidFormat(String operation) {
        return operation.trim().toLowerCase();
    }

    public static void showTaskMessage(String weekday) {
        String task = "";

        for (String[] schedule : schedules) {
            if (schedule[WEEKDAY_INDEX].equals(weekday)) {
                task = schedule[TASKS_INDEX];
                break;
            }
        }

        System.out.printf("Your tasks for %s: %s \n\n", weekday, task);
    }

    public static void rescheduleTaskOrShowError(String op) {
        String[] operations = op.split(" ");
        int VALID_OPERATION_LENGTH = 2;

        if (operations.length != VALID_OPERATION_LENGTH) {
            errorMessage();
            return;
        }

        String rescheduleText = operations[0];
        String weekday = operations[1];

        if (CHANGE.equals(rescheduleText) || RESCHEDULE.equals(rescheduleText)) {
            String[] weekdays = {SUNDAY, MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY};
            weekday = toValidFormat(weekday);

            int indexOfWeekday = -1;

            for (int i = 0; i < weekdays.length; i++) {
                if (weekdays[i].equals(weekday)) {
                    indexOfWeekday = i;
                    break;
                }
            }

            if (indexOfWeekday >= 0) {

                String task = "";

                while (true) {
                    System.out.println("Please, input the new task:");
                    task = scanner.nextLine();

                    if (task.equals("")) {
                        System.err.println("The task cannot be empty, try again:");
                        continue;
                    }

                    break;
                }

                schedules[indexOfWeekday][TASKS_INDEX] = task;

                return;
            }
        }

        errorMessage();
    }

    public static void errorMessage() {
        System.err.println("Sorry, I don't understand you, please try again.");
    }

}
