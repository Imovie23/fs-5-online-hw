package homework3;

import java.util.Scanner;

public class WeekTaskPlanner {
    public static final int DAYS_OF_WEEK = 7;
    public static final String SUNDAY = "sunday";
    public static final String MONDAY = "monday";
    public static final String TUESDAY = "tuesday";
    public static final String WEDNESDAY = "wednesday";
    public static final String THURSDAY = "thursday";
    public static final String FRIDAY = "friday";
    public static final String SATURDAY = "saturday";
    public static final String EXIT = "exit";
    public static String[][] schedules;

    public static void main(String[] args) {
        buildWeeklyTasks();
        tasksRunner();
    }

    public static void tasksRunner(){
        Scanner scanner = new Scanner(System.in);
        String operation;


        runner:
        while (true) {
            System.out.println("Please, input the day of the week:");

            operation = scanner.nextLine();
            operation = toValidFormat(operation);

            switch (operation) {
                case SUNDAY:
                    showTaskMessage(SUNDAY);
                    break;
                case MONDAY:
                    showTaskMessage(MONDAY);
                    break;
                case TUESDAY:
                    showTaskMessage(TUESDAY);
                    break;
                case WEDNESDAY:
                    showTaskMessage(WEDNESDAY);
                    break;
                case THURSDAY:
                    showTaskMessage(THURSDAY);
                    break;
                case FRIDAY:
                    showTaskMessage(FRIDAY);
                    break;
                case SATURDAY:
                    showTaskMessage(SATURDAY);
                    break;
                case EXIT:
                    System.out.println("Successful exit from the weekly task planner");
                    break runner;
                default:
                    System.err.println("Sorry, I don't understand you, please try again.");
                    break;
            }
        }
    }

    public static void buildWeeklyTasks() {
        String[][] weekSchedules = new String[DAYS_OF_WEEK][2];

        weekSchedules[0][0] = SUNDAY;
        weekSchedules[0][1] = "Sleep;";

        weekSchedules[1][0] = MONDAY;
        weekSchedules[1][1] = "Do home work;";

        weekSchedules[2][0] = TUESDAY;
        weekSchedules[2][1] = "Go to courses;";

        weekSchedules[3][0] = WEDNESDAY;
        weekSchedules[3][1] = "Watch a film;";

        weekSchedules[4][0] = THURSDAY;
        weekSchedules[4][1] = "Learn java;";

        weekSchedules[5][0] = FRIDAY;
        weekSchedules[5][1] = "Go to nature;";

        weekSchedules[6][0] = SATURDAY;
        weekSchedules[6][1] = "Walk with children;";

        schedules = weekSchedules;
    }

    public static String toValidFormat(String operation) {
        return operation.trim().toLowerCase();
    }

    public static void showTaskMessage(String weekday) {
        String task = "";

        for (String[] schedule : schedules) {
            if (schedule[0].equals(weekday)) {
                task = schedule[1];
                break;
            }
        }

        System.out.printf("Your tasks for %s: %s \n\n", weekday, task);
    }

}
