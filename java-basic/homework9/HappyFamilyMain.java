package homework9;

import homework9.dao.*;
import homework9.model.*;
import homework9.service.*;
import homework9.controller.*;
import java.util.*;

public class HappyFamilyMain {

    public static void main(String[] args) {
        FamilyDao familyDao = new CollectionFamilyDao();
        FamilyService familyService = new DefaultFamilyService(familyDao);
        FamilyController familyController = new FamilyController(familyService);

        Map<String, Set<String>> homerSchedule = new HashMap<>();
        homerSchedule.put(DayOfWeek.MONDAY.name(), new HashSet<>(List.of("Drink beer")));
        homerSchedule.put(DayOfWeek.FRIDAY.name(), new HashSet<>(List.of("Drink beer")));

        Map<String, Set<String>> margeSchedule = new HashMap<>();
        margeSchedule.put(DayOfWeek.MONDAY.name(), new HashSet<>(List.of("Clean kitchen")));

        Man homer = new Man("Homer", "Simpson", "25/02/1987", 55, homerSchedule);
        Woman marge = new Woman("Marge", "Simpson", "12/07/1999", 90, margeSchedule);

        Human bart = new Human("Bart", "Simpson", "10/11/2005", 100);

        familyController.createNewFamily(marge, homer);
        familyController.createNewFamily(bart, homer);

        Family family = familyController.getFamilyById(0);

        familyController.adoptChild(family, bart);


        System.out.println("Family with birth date");
        familyController.displayAllFamilies();

        System.out.println("Man with birth date");
        System.out.println(homer.describeAge());

        System.out.println("Woman with birth date");
        System.out.println(marge.describeAge());

        System.out.println("The child's date of birth");
        System.out.println(bart.describeAge());

    }
}
