package homework9.model;

public enum Species {
    ROBO_CAT, DOG, FISH, DOMESTIC_CAT, UNKNOWN
}
